package com.example.finalproject.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.finalproject.R;

public class AlertDialogUtils {

    public static void showAlertDialog(Context context, String title, String message){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .show();
    }

    public static void showAlertConfirmationDialog(Context context, String title, String message, DialogInterface.OnClickListener yesListener){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        yesListener.onClick(dialogInterface, i);
                    }
                })
                .setNegativeButton(R.string.no,  new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .show();
    }

    public static void showAlertOkDialog(Context context, String title, String message, DialogInterface.OnClickListener yesListener){
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        yesListener.onClick(dialogInterface, i);
                    }
                })
                .show();
    }
}
