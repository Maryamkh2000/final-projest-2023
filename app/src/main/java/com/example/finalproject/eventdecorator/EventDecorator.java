package com.example.finalproject.eventdecorator;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import androidx.core.content.ContextCompat;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;

public class EventDecorator implements DayViewDecorator {

    private final Drawable marker;
    private final int color;
    private final HashSet<CalendarDay> dates;

    public EventDecorator(Context context, int drawableResId, int color, Collection<CalendarDay> dates) {
        marker = ContextCompat.getDrawable(context, drawableResId);
        this.color = color;
        this.dates = new HashSet<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setBackgroundDrawable(marker);
        view.addSpan(new ForegroundColorSpan(color));
        view.addSpan(new StyleSpan(android.graphics.Typeface.BOLD));
        view.addSpan(new RelativeSizeSpan(1.2f));
    }
}
