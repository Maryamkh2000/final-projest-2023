package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.CheckedTeacherItem;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.adapters.SelectTeacherAdapter;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.interfaces.OnNextClicked;

import java.util.ArrayList;
import java.util.List;

public class SelectTeacherFragment extends Fragment implements OnNextClicked {

    private TextView title;
    private RecyclerView list;
    private SelectTeacherAdapter adapter;
    private List<CheckedTeacherItem> listItems = new ArrayList<>();
    private CheckedTeacherItem checkedTeacher;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_lesson_list_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title = view.findViewById(R.id.title);
        title.setText(R.string.please_selet_teacher);
        list = view.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter = new SelectTeacherAdapter(new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                CheckedTeacherItem selected =  listItems.get(position);
                if (selected == checkedTeacher) {
                    return;
                } else {
                    selected.setChecked(true);
                    checkedTeacher.setChecked(false);
                    checkedTeacher = selected;
                }
                adapter.notifyDataSetChanged();
            }
        });
        list.setAdapter(adapter);
        initItems();
    }

    private void initItems() {
        listItems.clear();
        ArrayList<Teacher> teachers = (ArrayList<Teacher>)getArguments().getSerializable("teachers");
        for (Teacher teacher : teachers) {
            listItems.add(new CheckedTeacherItem(false, teacher));
        }
        listItems.get(0).setChecked(true);
        checkedTeacher = listItems.get(0);
        adapter.setItems(listItems);
    }
    @Override
    public void onNextClicked(Lesson lesson) {
        lesson.setTeacher(checkedTeacher.getTeacher().copy());
        ((HasNext)this.getActivity()).goNext();
    }
}
