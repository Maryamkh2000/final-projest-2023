package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SelectDateFragment extends Fragment implements OnNextClicked {

    private RadioGroup radioGroup;
    private TextView title;
    private long selected;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private ArrayList<Long> dates;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((HasBack)getActivity()).handlePrevButton();
        super.onViewCreated(view, savedInstanceState);
        radioGroup = view.findViewById(R.id.radioGroup);
        title = view.findViewById(R.id.title);
        title.setText(R.string.please_select_date);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) view.findViewById(checkedId);
                selected = dates.get(group.indexOfChild(radioButton));
            }
        });
        dates = (ArrayList<Long>)(getArguments().getSerializable("dates"));
        initRadioGroup(dates);
    }

    private void initRadioGroup(List<Long> items) {
        for (int i = 0; i < items.size(); i++) {
            RadioButton rb = new RadioButton(this.getContext());
            rb.setText(format.format(new Date(items.get(i))));
            rb.setTextSize(24f);
            radioGroup.addView(rb);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) rb.getLayoutParams();
            marginLayoutParams.bottomMargin = 42;
        }
        radioGroup.check(radioGroup.getChildAt(0).getId());
    }

    @Override
    public void onNextClicked(Lesson lesson) {
        lesson.setTime(selected);
        ((HasNext)this.getActivity()).goNext();
    }
}
