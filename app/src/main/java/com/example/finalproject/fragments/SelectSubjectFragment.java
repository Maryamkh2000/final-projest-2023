package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.Subjects;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import com.example.finalproject.session.SessionUser;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class SelectSubjectFragment extends Fragment implements OnNextClicked {

    public static final String SELECTED_SUBJECT = "SELECTED_SUBJECT";
    private RadioGroup radioGroup;
    private TextView title;
    private String selected;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((HasBack)getActivity()).handlePrevButton();
        super.onViewCreated(view, savedInstanceState);
        radioGroup = view.findViewById(R.id.radioGroup);
        title = view.findViewById(R.id.title);
        title.setText(R.string.please_select_a_subject);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) view.findViewById(checkedId);
                selected = radioButton.getText().toString();
            }
        });
        initSubjects();
    }

    private void initSubjects() {
        if (SessionUser.isStudent() && getArguments() != null && getArguments().getSerializable("subjects") != null) {
            initRadioGroup((List<String>)(getArguments().getSerializable("subjects")));
        } else {
            FirebaseDatabaseManager.getSubjects(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    Subjects subjects = documentSnapshot.toObject(Subjects.class);
                    initRadioGroup(new ArrayList<String>(subjects.getTypes().values()));
                }
            });
        }

    }

    private void initRadioGroup(List<String> items) {
        String selected = getArguments() != null ? getArguments().getString(SELECTED_SUBJECT, null) : null;
        selected = selected == null && this.selected != null ? this.selected : selected;
        int selectedPosition = 0;
        for (int i = 0; i < items.size(); i++) {
            RadioButton rb = new RadioButton(this.getContext());
            rb.setText(items.get(i));
            rb.setTextSize(24f);
            radioGroup.addView(rb);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) rb.getLayoutParams();
            marginLayoutParams.bottomMargin = 42;
            if (items.get(i).equals(selected)) {
                selectedPosition = i;
            }
        }
        radioGroup.check(radioGroup.getChildAt(selectedPosition).getId());
    }

    @Override
    public void onNextClicked(Lesson lesson) {
        lesson.setSubject(selected);
        ((HasNext)this.getActivity()).goNext();
    }
}
