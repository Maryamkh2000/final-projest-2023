package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import com.example.finalproject.utils.AlertDialogUtils;

public class PaymentFragment extends Fragment implements OnNextClicked {

    private EditText phone, date, cardNumber, firstName, lastName, cvv, email;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.payment_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((HasBack)getActivity()).handlePrevButton();
        super.onViewCreated(view, savedInstanceState);
        phone = view.findViewById(R.id.phone);
        date = view.findViewById(R.id.date);
        cardNumber = view.findViewById(R.id.cardNumber);
        firstName = view.findViewById(R.id.firstName);
        lastName = view.findViewById(R.id.lastName);
        cvv = view.findViewById(R.id.cvv);
        email = view.findViewById(R.id.email);


    }

    @Override
    public void onNextClicked(Lesson lesson) {
        if (firstName.getText().toString().equals("")) {
            AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.please_enter_your_first_name));
        } else if (lastName.getText().toString().equals("")) {
            AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.please_enter_your_last_name));
        } else if (email.getText().toString().equals("")) {
            AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.please_enter_your_email));
        } else if (cardNumber.getText().length() < 16) {
            AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.please_enter_more_card));
        } else if (phone.getText().length() < 10) {
            AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.please_enter_more_phone_digits));
        } else if (cvv.getText().length() < 3) {
            AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.please_enter_more_cvv_digits));
        } else if (date.getText().toString().equals("")) {
            AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.please_enter_expire));
        } else {
            ((HasNext)this.getActivity()).goNext();
        }
    }
}
