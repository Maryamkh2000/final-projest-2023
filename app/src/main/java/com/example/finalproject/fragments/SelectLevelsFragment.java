package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import java.util.Arrays;
import java.util.List;

public class SelectLevelsFragment extends Fragment implements OnNextClicked {

    public static final String SELECTED_LEVEL= "SELECTED_LEVEL";
    private RadioGroup radioGroup;
    private TextView title;
    private String selected;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HasBack)getActivity()).handlePrevButton();
        radioGroup = view.findViewById(R.id.radioGroup);
        title = view.findViewById(R.id.title);
        title.setText(R.string.please_select_lesson_level);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) view.findViewById(checkedId);
                selected = radioButton.getText().toString();
            }
        });
        if (getArguments() != null &&  getArguments().getSerializable("levels") != null) {
            initRadioGroup((List<Integer>) getArguments().getSerializable("levels"));
        } else {
            initRadioGroup(Arrays.asList(1,2,3,4,5,6));
        }
    }

    private void initRadioGroup(List<Integer> items) {
        int selectedLevel = getArguments() != null ? getArguments().getInt(SELECTED_LEVEL, -1) : -1;
        int selectedPosition = 0;
        for (int i = 0; i < items.size(); i++) {
            RadioButton rb = new RadioButton(this.getContext());
            rb.setId(i);
            rb.setText(String.valueOf(items.get(i)));
            rb.setTextSize(24f);
            radioGroup.addView(rb);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) rb.getLayoutParams();
            marginLayoutParams.bottomMargin = 42;
            if (selectedLevel == items.get(i)) {
                selectedPosition = i;
            }
        }
        radioGroup.check(radioGroup.getChildAt(selectedPosition).getId());
    }

    @Override
    public void onNextClicked(Lesson lesson) {
        lesson.setLevel(Integer.parseInt(selected));
        ((HasNext)this.getActivity()).goNext();
    }
}
