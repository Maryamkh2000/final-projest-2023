package com.example.finalproject.fragments;

import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.CheckedSchoolItem;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.School;
import com.example.finalproject.R;
import com.example.finalproject.adapters.SelectSchoolAdapter;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.interfaces.OnNextClicked;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SelectSchoolFragment extends Fragment implements OnNextClicked {

    public static final String SELECTED_SCHOOL = "SELECTED_SCHOOL";
    private TextView title;
    private RecyclerView list;
    private SelectSchoolAdapter adapter;
    private List<CheckedSchoolItem> listItems = new ArrayList<>();
    private CheckedSchoolItem checkedSchoolItem;
    private FusedLocationProviderClient fusedLocationClient;
    private Location location;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_lesson_list_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HasBack)getActivity()).handlePrevButton();
        listItems.clear();
        title = view.findViewById(R.id.title);
        title.setText(R.string.please_select_school);
        list = view.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter = new SelectSchoolAdapter(new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                CheckedSchoolItem selected =  listItems.get(position);
                if (selected == checkedSchoolItem) {
                    return;
                } else {
                    selected.setChecked(true);
                    checkedSchoolItem.setChecked(false);
                    checkedSchoolItem = selected;
                }
                adapter.notifyDataSetChanged();
            }
        });
        list.setAdapter(adapter);
        findMyLocationIfHasAccess();
    }


    private void fetchSchools() {
        if (getArguments() == null || getArguments().getSerializable("schools") == null) {
            initAllSchools();
        } else {
            ArrayList<School> schools = (ArrayList<School>) getArguments().getSerializable("schools");
            for (School school : schools) {
                CheckedSchoolItem checkedSchoolItem = new CheckedSchoolItem(false, school);
                if (school.getGeoPoint() != null) {
                    checkedSchoolItem.setDistance(getDistance(checkedSchoolItem.getSchool()));
                }
                listItems.add(checkedSchoolItem);
            }
            if (!listItems.isEmpty()) {
                listItems.get(0).setChecked(true);
                checkedSchoolItem =  listItems.get(0);
            }
            orderByMyLocation();
            adapter.setItems(listItems);
        }
    }

    private void initAllSchools() {
        FirebaseDatabaseManager.getAllSchools(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<DocumentSnapshot> documentSnapshots = task.getResult().getDocuments();
                    School checkSchool = getArguments() != null ? (School) getArguments().getSerializable(SELECTED_SCHOOL) : null;
                    checkSchool = checkSchool == null && checkedSchoolItem != null ? checkedSchoolItem.getSchool() : checkSchool;
                    for (DocumentSnapshot document : documentSnapshots) {
                        CheckedSchoolItem item = new CheckedSchoolItem(false, document.toObject(School.class));
                        if (item.getSchool().getGeoPoint() != null) {
                            item.setDistance(getDistance(item.getSchool()));
                        }
                        listItems.add(item);
                        if (item.getSchool().equals(checkSchool)) {
                            checkedSchoolItem = item;
                            item.setChecked(true);
                        }
                    }
                    if (checkedSchoolItem == null && !listItems.isEmpty()) {
                        checkedSchoolItem = listItems.get(0);
                        checkedSchoolItem.setChecked(true);
                    }
                    orderByMyLocation();
                    adapter.setItems(listItems);
                }
            }
        });
    }

    private void findMyLocationIfHasAccess() {
        if (ActivityCompat.checkSelfPermission(this.getContext(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        } else {
            findMyLocation();
        }
    }

    private void findMyLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            SelectSchoolFragment.this.location = location;
                            fetchSchools();
                        }
                    });
        }
    }

    private void orderByMyLocation() {
        listItems.sort(new Comparator<CheckedSchoolItem>() {
            @Override
            public int compare(CheckedSchoolItem o1, CheckedSchoolItem o2) {
                return Float.compare(o1.getDistance(), o2.getDistance());
            }
        });

    }

    private float getDistance(School school) {
        float distance = -1;

        Location schoolLocation = new Location("location");
        schoolLocation.setLatitude(school.getGeoPoint().getLatitude());
        schoolLocation.setLongitude(school.getGeoPoint().getLongitude());
        if (this.location != null) {
            distance = (schoolLocation.distanceTo(this.location)) / 1000;
        }

        return distance;
    }
    @Override
    public void onNextClicked(Lesson lesson) {
        lesson.setSchool(checkedSchoolItem.getSchool());
        ((HasNext)this.getActivity()).goNext();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                findMyLocation();
            }
        }
    }
}
