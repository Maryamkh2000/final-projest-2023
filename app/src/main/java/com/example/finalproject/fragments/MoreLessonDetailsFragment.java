package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import com.example.finalproject.utils.AlertDialogUtils;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class MoreLessonDetailsFragment extends Fragment implements OnNextClicked {

    public static final String BRIEF = "BRIEF";
    public static final String DURATION = "DURATION";
    public static final String PRICE = "PRICE";
    public static final String MAX = "MAX";

    private EditText briefET, priceET, durationET;
    private TextView zoomLink;
    private Spinner maxSpinner;
    private Switch zoomSwitch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.more_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ((HasBack)getActivity()).handlePrevButton();
        super.onViewCreated(view, savedInstanceState);
        briefET = view.findViewById(R.id.brief);
        durationET = view.findViewById(R.id.durationET);
        maxSpinner = view.findViewById(R.id.maxSpinner);
        priceET = view.findViewById(R.id.price);
        zoomLink = view.findViewById(R.id.zoomLink);
        zoomSwitch = view.findViewById(R.id.zoomSwitch);
        zoomSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked) {
                    zoomLink.setText("https://studyApp.zoom/" + System.currentTimeMillis());
                    zoomLink.setVisibility(View.VISIBLE);
                }else {
                    zoomLink.setVisibility(View.GONE);
                }
            }
        });
        if (getArguments() != null) {
            briefET.setText(getArguments().getString(BRIEF));
            priceET.setText(getArguments().getString(PRICE));
            durationET.setText(String.valueOf(getArguments().getInt(DURATION)));
            String[] stringArray2 = getResources().getStringArray(R.array.MaxStudents);
            for (int i = 0; i< stringArray2.length ;i++) {
                if (stringArray2[i].equals(getArguments().getString(MAX))){
                    maxSpinner.setSelection(i);
                    break;
                }
            }

        }

    }

    @Override
    public void onNextClicked(Lesson lesson) {
        lesson.setZoomLink(zoomSwitch.isChecked() ? zoomLink.getText().toString() : null);
        lesson.setBriefInformation(briefET.getText().toString());
        lesson.setStudentsLimit(Integer.parseInt(maxSpinner.getSelectedItem().toString()));
        long time = lesson.getTime() +  TimeUnit.MINUTES.toMillis(durationET.getText().toString().equals("") ? 0 : Long.parseLong(durationET.getText().toString()) );
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        if (priceET.getText().toString().equals("")) {
            AlertDialogUtils.showAlertDialog(this.getActivity(), getString(R.string.error), getString(R.string.enter_price));
        }else if (durationET.getText().toString().equals("0") || durationET.getText().toString().equals("")) {
            AlertDialogUtils.showAlertDialog(this.getActivity(), getString(R.string.error), getString(R.string.please_enter_duration));
        } else if (calendar.get(Calendar.HOUR_OF_DAY) > lesson.getSchool().getClosingHour()) {
            AlertDialogUtils.showAlertDialog(this.getActivity(), getString(R.string.error), getString(R.string.your_time_exceeds));
        } else {
            lesson.setPrice(Double.parseDouble(priceET.getText().toString()));
            lesson.setDuration(Integer.parseInt(durationET.getText().toString()));
            ((HasNext)this.getActivity()).goNext();

        }
    }
}
