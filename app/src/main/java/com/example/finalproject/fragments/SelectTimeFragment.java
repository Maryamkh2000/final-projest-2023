package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.School;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import com.example.finalproject.utils.AlertDialogUtils;

import java.util.Calendar;

public class SelectTimeFragment extends Fragment implements OnNextClicked {

    private CalendarView calendarView;
    private TimePicker timePicker;
    Calendar calendar;
    School school;
    int hour = -1;
    int min = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.choose_time_teacher_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((HasBack)getActivity()).handlePrevButton();
        calendarView = view.findViewById(R.id.calender);
        timePicker = view.findViewById(R.id.timePicker);
        school = (School) getArguments().getSerializable("school");
        calendar = Calendar.getInstance();
        timePicker.setIs24HourView(true);
        hour = timePicker.getHour();
        min = timePicker.getMinute();
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
                calendar = Calendar.getInstance();
                calendar.set(year, month, dayOfMonth);
            }
        });
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int min) {
                SelectTimeFragment.this.hour = hour;
                SelectTimeFragment.this.min = min;
            }
        });
    }


    @Override
    public void onNextClicked(Lesson lesson) {
      if (hour < school.getOpeningHour() || hour > school.getClosingHour()) {
          AlertDialogUtils.showAlertDialog(getContext(), getString(R.string.error), getString(R.string.please_select_hour_in_this_range, school.getOpeningHour(), school.getClosingHour()));
      } else {
          calendar.set(Calendar.HOUR_OF_DAY, hour);
          calendar.set(Calendar.MINUTE, min);
          if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
              AlertDialogUtils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.the_time_you_chose_has_passed));
          } else {
              lesson.setTime(calendar.getTimeInMillis());
              ((HasNext)getActivity()).goNext();
          }
      }
    }
}
