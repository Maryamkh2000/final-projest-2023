//package com.example.finalproject.workmanager;
//
//
//import android.content.Context;
//
//import androidx.annotation.NonNull;
//import androidx.work.Worker;
//import androidx.work.WorkerParameters;
//
//import com.example.finalproject.R;
//import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;
//
//public class SendLessonNotificationWorker extends Worker {
//
//    public SendLessonNotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
//        super(context, workerParams);
//    }
//
//    @NonNull
//    @Override
//    public Result doWork() {
//        String lessonId = getInputData().getString("id");
//        FirebaseMessagingManager.sendNotification(getApplicationContext(), lessonId,
//                getApplicationContext().getString(R.string.the_lesson_will_start_in_10_minutes) );
//        return Result.success();
//    }
//}
