package com.example.finalproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import com.example.finalproject.Logic.Lesson;

public class ResultClassesAdapter extends RecyclerView.Adapter<ResultClassesHolder> {

    Context context;
    ArrayList<Lesson> lessons;

    public ResultClassesAdapter(Context applicationContext, ArrayList<Lesson> lessons) {
        this.context = applicationContext;
        this.lessons = lessons;
    }

    @NonNull
    @Override
    public ResultClassesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ResultClassesHolder(LayoutInflater.from(context).inflate(R.layout.result_class_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ResultClassesHolder holder, int position) {
        holder.info1.setText(lessons.get(position).getBriefInformation());
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }
}
