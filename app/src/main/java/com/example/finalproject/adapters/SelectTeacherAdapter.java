package com.example.finalproject.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalproject.Logic.CheckedTeacherItem;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;

import java.util.ArrayList;
import java.util.List;

public class SelectTeacherAdapter extends RecyclerView.Adapter<SelectTeacherAdapter.SelectedTeacherVM> {

    private List<CheckedTeacherItem> items = new ArrayList<>();
    private OnClickListener onClickListener;

    public void setItems(List<CheckedTeacherItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public SelectTeacherAdapter(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public SelectedTeacherVM onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SelectedTeacherVM(LayoutInflater.from(parent.getContext()).inflate(R.layout.checked_teacher, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedTeacherVM holder, int position) {
        Teacher teacher = items.get(position).getTeacher();
        holder.name.setText(teacher.getName());
        Glide.with(holder.itemView.getContext())
                .load(teacher.getImageUrl()).circleCrop()
                .placeholder(R.drawable.baseline_person_24).into(holder.profileImage);
        holder.checkBox.setChecked(items.get(position).isChecked());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class SelectedTeacherVM extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView profileImage;
        private CheckBox checkBox;


        public SelectedTeacherVM(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            profileImage = itemView.findViewById(R.id.profileImage);
            checkBox = itemView.findViewById(R.id.checkbox);
        }

    }
}
