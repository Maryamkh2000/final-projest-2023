package com.example.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalproject.Logic.Feedback;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;

import java.util.List;

public class FeedbacksAdapter extends RecyclerView.Adapter<FeedbacksAdapter.LessonsHolder> {

    private Context context;
    private List<Feedback> feedbacks;
    private OnClickListener onAttachmentListener;

    public void setItems(List<Feedback> feedbacks) {
        this.feedbacks = feedbacks;
        notifyDataSetChanged();
    }

    public FeedbacksAdapter(Context context, List<Feedback> feedbacks, OnClickListener onAttachmentListener) {
        this.context = context;
        this.feedbacks = feedbacks;
        this.onAttachmentListener = onAttachmentListener;
    }

    @NonNull
    @Override
    public LessonsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LessonsHolder(LayoutInflater.from(context).inflate(R.layout.feedback_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LessonsHolder holder, int position) {
        Feedback feedback = feedbacks.get(position);
        Glide.with(holder.itemView.getContext())
                .load(feedback.getTeacher().getImageUrl()).circleCrop()
                .placeholder(R.drawable.baseline_person_24).into(holder.image);

        holder.name.setText(feedback.getTeacher().getName());
        holder.message.setText(feedback.getMessage());
        holder.attachment.setVisibility(feedback.getAttachment() != null ? View.VISIBLE : View.GONE);
        holder.attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAttachmentListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return feedbacks.size();
    }

    public class LessonsHolder extends RecyclerView.ViewHolder {

        TextView message, name;
        ImageView image, attachment;

        public LessonsHolder(@NonNull View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message);
            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);
            attachment = itemView.findViewById(R.id.attachment);
        }
    }
}
