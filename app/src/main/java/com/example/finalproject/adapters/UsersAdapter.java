package com.example.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalproject.Logic.User;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.session.SessionUser;

import java.text.SimpleDateFormat;
import java.util.List;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.UssrViewHolder> {

    private Context context;
    private List<User> users;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private boolean showRegisterButton = false;
    private OnClickListener addFeedbackListener;
    private OnClickListener sendEmailListener;

    public UsersAdapter(Context context, List<User> users,
                        OnClickListener addFeedbackListener, OnClickListener sendEmailListener) {
        this.context = context;
        this.users = users;
        this.addFeedbackListener = addFeedbackListener;
        this.sendEmailListener = sendEmailListener;
    }

    @NonNull
    @Override
    public UssrViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UssrViewHolder(LayoutInflater.from(context).inflate(R.layout.user_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UssrViewHolder holder, int position) {
        User user = users.get(position);
        Glide.with(context)
                .load(user.getImageUrl()).circleCrop()
                .placeholder(R.drawable.baseline_person_24).into(holder.profileImage);
        holder.name.setText(user.getName());
        holder.phone.setText(user.getPhone());
        holder.email.setText(user.getEmail());
        holder.age.setText(user.getAge());
        holder.gender.setText(user.getGender());
        holder.address.setText(user.getRegion());
        holder.feedbackBT.setVisibility(SessionUser.isTeacher() ? View.VISIBLE : View.GONE);
        holder.feedbackBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFeedbackListener.onItemClicked(position);
            }
        });
        holder.emailBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmailListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class UssrViewHolder extends RecyclerView.ViewHolder {

        TextView name, address, age, phone, email, gender;
        ImageView profileImage, feedbackBT, emailBT;

        public UssrViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            age = itemView.findViewById(R.id.age);
            phone = itemView.findViewById(R.id.phone);
            email = itemView.findViewById(R.id.email);
            gender = itemView.findViewById(R.id.gender);
            profileImage = itemView.findViewById(R.id.profileImage);
            feedbackBT = itemView.findViewById(R.id.feedbackBT);
            emailBT = itemView.findViewById(R.id.emailBT);
        }
    }
}
