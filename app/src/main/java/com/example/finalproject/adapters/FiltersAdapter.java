package com.example.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.CheckedFilterItem;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.interfaces.OnFilterClicked;

import java.text.SimpleDateFormat;
import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.FilterViewHolder> {

    private Context context;
    private List<CheckedFilterItem> items;
    private OnFilterClicked onClickListener;
    private OnClickListener editListener;
    private OnClickListener onShowStudentsListener;
    private OnClickListener showReviewListener;

    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public FiltersAdapter(Context context, List<CheckedFilterItem> items, OnFilterClicked onClickListener) {
        this.context = context;
        this.items = items;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public FilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FilterViewHolder(LayoutInflater.from(context).inflate(R.layout.filter_checked_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilterViewHolder holder, int position) {
        holder.title.setText(items.get(position).getTitle());
        holder.subtitle.setText(items.get(position).getSubtitle());
        holder.checked.setVisibility(items.get(position).isChecked() ? View.VISIBLE : View.INVISIBLE);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onFilterClicked(items.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class FilterViewHolder extends RecyclerView.ViewHolder {

        TextView title, subtitle;
        Button studentsBT, reviewBT;
        ImageView checked, editBT;

        public FilterViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            subtitle = itemView.findViewById(R.id.subtitle);
            checked = itemView.findViewById(R.id.checked);
        }
    }
}
