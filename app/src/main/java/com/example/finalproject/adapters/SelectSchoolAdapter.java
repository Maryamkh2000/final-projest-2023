package com.example.finalproject.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.CheckedSchoolItem;
import com.example.finalproject.Logic.School;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;

import java.util.ArrayList;
import java.util.List;

public class SelectSchoolAdapter extends RecyclerView.Adapter<SelectSchoolAdapter.SelectedSchoolVM> {

    private List<CheckedSchoolItem> items = new ArrayList<>();
    private OnClickListener onClickListener;

    public void setItems(List<CheckedSchoolItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public SelectSchoolAdapter(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public SelectedSchoolVM onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SelectedSchoolVM(LayoutInflater.from(parent.getContext()).inflate(R.layout.checked_school, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedSchoolVM holder, int position) {
        School school = items.get(position).getSchool();
        holder.name.setText(school.getName());
        holder.location.setText(school.getLocation());
        holder.hours.setText(school.getOpeningHour() + "-" + school.getClosingHour());
        holder.checkBox.setChecked(items.get(position).isChecked());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onItemClicked(position);
            }
        });
        holder.distance.setText(String.format("%.2f", items.get(position).getDistance()) + " km");
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class SelectedSchoolVM extends RecyclerView.ViewHolder {
        private TextView name, location, hours, distance;
        private CheckBox checkBox;


        public SelectedSchoolVM(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            location = itemView.findViewById(R.id.location);
            hours = itemView.findViewById(R.id.hours);
            distance = itemView.findViewById(R.id.distance);
            checkBox = itemView.findViewById(R.id.checkbox);
        }

    }
}
