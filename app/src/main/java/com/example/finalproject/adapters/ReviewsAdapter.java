package com.example.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.finalproject.Logic.LessonReview;
import com.example.finalproject.R;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.interfaces.OnClickListener;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsHolder> {

    private Context context;
    private List<LessonReview> reviews;
    private OnClickListener deleteListener;

    public ReviewsAdapter(Context context, List<LessonReview> reviews, OnClickListener deleteListener) {
        this.context = context;
        this.reviews = reviews;
        this.deleteListener = deleteListener;
    }

    @NonNull
    @Override
    public ReviewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReviewsHolder(LayoutInflater.from(context).inflate(R.layout.review_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsHolder holder, int position) {
        LessonReview lessonReview = reviews.get(position);
        holder.name.setText(lessonReview.getReviewer().getName());
        Glide.with(context)
                .load(lessonReview.getReviewer().getImageUrl()).circleCrop()
                .placeholder(R.drawable.baseline_person_24).into(holder.profileImage);
        holder.ratingBar.setRating(lessonReview.getRating());
        holder.review.setText(lessonReview.getComment());
        holder.removeBT.setVisibility(lessonReview.getReviewer().equals(SessionUser.currentUser) ? View.VISIBLE : View.GONE);
        holder.removeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public class ReviewsHolder extends RecyclerView.ViewHolder {

        TextView review, name;
        RatingBar ratingBar;
        ImageView profileImage, removeBT;

        public ReviewsHolder(@NonNull View itemView) {
            super(itemView);
            profileImage = itemView.findViewById(R.id.profileImage);
            ratingBar = itemView.findViewById(R.id.ratingBar);
            name = itemView.findViewById(R.id.name);
            review = itemView.findViewById(R.id.review);
            removeBT = itemView.findViewById(R.id.removeBT);
        }
    }
}
