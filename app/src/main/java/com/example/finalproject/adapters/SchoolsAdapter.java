package com.example.finalproject.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.School;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;

import java.util.ArrayList;
import java.util.List;

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.SchoolVM> {

    private List<School> items = new ArrayList<>();
    private OnClickListener onClickListener;

    public void setItems(List<School> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SchoolVM onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SchoolVM(LayoutInflater.from(parent.getContext()).inflate(R.layout.school_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolVM holder, int position) {
        School school = items.get(position);
        holder.name.setText(school.getName());
        holder.location.setText(school.getLocation());
        holder.hours.setText(school.getOpeningHour() + "-" + school.getClosingHour());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class SchoolVM extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView location;
        private TextView hours;


        public SchoolVM(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            location = itemView.findViewById(R.id.location);
            hours = itemView.findViewById(R.id.hours);
        }

    }
}
