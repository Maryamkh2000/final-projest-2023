package com.example.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.session.SessionUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LessonsAdapter extends RecyclerView.Adapter<LessonsAdapter.LessonsHolder> {

    private final OnClickListener teacherListener;
    private OnClickListener zoomLinkListener;
    private Context context;
    private List<Lesson> lessons;
    private OnClickListener deleteListener;
    private OnClickListener editListener;
    private OnClickListener onShowStudentsListener;
    private OnClickListener showReviewListener;
    private OnClickListener uploadFileListener;
    private OnClickListener openFileListener;
    private final OnClickListener feedbacksListener;
    private final OnClickListener emailListener;


    public void setItems(List<Lesson> lessons) {
        this.lessons = lessons;
        notifyDataSetChanged();
    }

    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public LessonsAdapter(Context context, List<Lesson> lessons, OnClickListener deleteListener,
                          OnClickListener editListener, OnClickListener onShowStudentsListener
            , OnClickListener showReviewListener, OnClickListener uploadFile,
                          OnClickListener openFile, OnClickListener showFeedbacks,
                          OnClickListener zoomLink, OnClickListener emailListener,
                          OnClickListener teacherListener) {
        this.context = context;
        this.lessons = lessons;
        this.deleteListener = deleteListener;
        this.editListener = editListener;
        this.onShowStudentsListener = onShowStudentsListener;
        this.showReviewListener = showReviewListener;
        this.uploadFileListener = uploadFile;
        this.openFileListener = openFile;
        this.feedbacksListener = showFeedbacks;
        this.zoomLinkListener = zoomLink;
        this.emailListener = emailListener;
        this.teacherListener = teacherListener;
    }

    @NonNull
    @Override
    public LessonsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LessonsHolder(LayoutInflater.from(context).inflate(R.layout.lesson_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LessonsHolder holder, int position) {
        Lesson lesson = lessons.get(position);
        holder.subject.setText(lesson.getSubject());
        holder.duration.setText(context.getString(R.string.number_mins, lesson.getDuration()));
        holder.level.setText(String.valueOf(lesson.getLevel()));
        holder.school.setText(lesson.getSchool().getName());
        holder.removeBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteListener.onItemClicked(holder.getAdapterPosition());
            }
        });
        holder.date.setText(format.format(new Date(lesson.getTime())));
        holder.studentsBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onShowStudentsListener != null) {
                    onShowStudentsListener.onItemClicked(holder.getAdapterPosition());
                }
            }
        });
        holder.editBT.setVisibility(SessionUser.isTeacher() ? View.VISIBLE : View.GONE);
        holder.editBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editListener != null) {
                    editListener.onItemClicked(holder.getAdapterPosition());
                }
            }
        });
        holder.removeBT.setVisibility(SessionUser.isAdmin() ? View.GONE : View.VISIBLE);
        holder.reviewBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showReviewListener != null) {
                    showReviewListener.onItemClicked(position);
                }
            }
        });
        holder.uploadFileBT.setVisibility(SessionUser.isTeacher() ? View.VISIBLE : View.GONE);
        holder.uploadFileBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadFileListener.onItemClicked(position);
            }
        });
        holder.fileBT.setVisibility(lesson.getFile() != null ? View.VISIBLE : View.GONE);
        holder.fileBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileListener.onItemClicked(position);
            }
        });
        holder.max.setText(String.valueOf(lesson.getStudentsLimit()) );
        holder.price.setText(String.valueOf(lesson.getPrice()));
        holder.feedbacksBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feedbacksListener.onItemClicked(position);
            }
        });
        holder.feedbacksBT.setVisibility(SessionUser.isStudent() ? View.VISIBLE : View.GONE);
        holder.zoomContainer.setVisibility(lesson.getZoomLink() != null ? View.VISIBLE : View.GONE);
        holder.zoomLink.setText(lesson.getZoomLink());
        holder.zoomLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zoomLinkListener.onItemClicked(position);
            }
        });
        holder.emailBT.setVisibility(SessionUser.isTeacher() ? View.GONE : View.VISIBLE);
        holder.emailBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailListener.onItemClicked(position);
            }
        });
        holder.showTeacherBT.setVisibility(SessionUser.isTeacher() ? View.GONE : View.VISIBLE);
        holder.showTeacherBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                teacherListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }

    public class LessonsHolder extends RecyclerView.ViewHolder {

        TextView subject, date, level, duration, teacher, school, price, max, zoomLink;
        Button studentsBT, reviewBT, feedbacksBT, showTeacherBT;
        ImageView removeBT, editBT, fileBT, uploadFileBT, emailBT;
        LinearLayout zoomContainer;

        public LessonsHolder(@NonNull View itemView) {
            super(itemView);

            subject = itemView.findViewById(R.id.subject);
            date = itemView.findViewById(R.id.date);
            level = itemView.findViewById(R.id.level);
            duration = itemView.findViewById(R.id.duration);
            teacher = itemView.findViewById(R.id.teacher);
            school = itemView.findViewById(R.id.school);
            studentsBT = itemView.findViewById(R.id.show_students);
            reviewBT = itemView.findViewById(R.id.show_reviews);
            removeBT = itemView.findViewById(R.id.removeBT);
            feedbacksBT = itemView.findViewById(R.id.show_feedbacks);
            editBT = itemView.findViewById(R.id.editBT);
            fileBT = itemView.findViewById(R.id.fileBT);
            uploadFileBT = itemView.findViewById(R.id.uploadFileBT);
            price = itemView.findViewById(R.id.price);
            max = itemView.findViewById(R.id.studentLimit);
            zoomContainer = itemView.findViewById(R.id.zoomContainer);
            zoomLink = itemView.findViewById(R.id.zoomLink);
            emailBT = itemView.findViewById(R.id.emailBT);
            showTeacherBT = itemView.findViewById(R.id.showTeacher);
        }
    }
}
