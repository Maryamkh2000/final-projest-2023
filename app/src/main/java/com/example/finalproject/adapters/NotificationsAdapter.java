package com.example.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.Notification;
import com.example.finalproject.R;
import com.example.finalproject.interfaces.OnClickListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationHolder> {

    private Context context;
    private List<Notification> notifications;
    private OnClickListener onDeleteListener;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public void setItems(List<Notification> Notifications) {
        this.notifications = Notifications;
        notifyDataSetChanged();
    }

    public NotificationsAdapter(Context context, List<Notification> notifications, OnClickListener deleteListener) {
        this.context = context;
        this.notifications = notifications;
        this.onDeleteListener = deleteListener;
    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationHolder(LayoutInflater.from(context).inflate(R.layout.notification_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder holder, int position) {
        Notification notification = notifications.get(position);

        holder.title.setText(notification.getTitle());
        holder.message.setText(notification.getBody());
        holder.time.setText(format.format(new Date(notification.getTime())));

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteListener.onItemClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {

        TextView message, time, title;
        ImageView delete;

        public NotificationHolder(@NonNull View itemView) {
            super(itemView);

            message = itemView.findViewById(R.id.message);
            title = itemView.findViewById(R.id.title);
            time = itemView.findViewById(R.id.time);
            delete = itemView.findViewById(R.id.delete);
        }
    }
}
