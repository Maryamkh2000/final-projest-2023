package com.example.finalproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LessonInCalenderAdapter extends RecyclerView.Adapter<LessonInCalenderAdapter.LessonVM> {

    private Context context;
    private List<Lesson> lessons;
    private SimpleDateFormat format = new SimpleDateFormat("HH:mm");


    public void  setItems(List<Lesson> items) {
        this.lessons = items;
        notifyDataSetChanged();
    }

    public LessonInCalenderAdapter(Context context, List<Lesson> lessons) {
        this.context = context;
        this.lessons = lessons;
    }

    @NonNull
    @Override
    public LessonVM onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LessonVM(LayoutInflater.from(context).inflate(R.layout.lesson_calender_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LessonVM holder, int position) {
        Lesson lesson= lessons.get(position);
        holder.level.setText(String.valueOf(lesson.getLevel()));
        holder.subject.setText(lesson.getSubject());
        holder.school.setText(lesson.getSchool().getName());
        holder.duration.setText(context.getString(R.string.number_mins, lesson.getDuration()));
        holder.time.setText(format.format(new Date(lesson.getTime())));

    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }

    public class LessonVM extends RecyclerView.ViewHolder {

        TextView time, school, level, subject, duration;

        public LessonVM(@NonNull View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            school = itemView.findViewById(R.id.school);
            level = itemView.findViewById(R.id.level);
            duration = itemView.findViewById(R.id.duration);
            subject = itemView.findViewById(R.id.subject);
        }
    }
}
