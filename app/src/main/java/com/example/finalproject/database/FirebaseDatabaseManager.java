package com.example.finalproject.database;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.finalproject.Logic.Feedback;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.LessonRequest;
import com.example.finalproject.Logic.LessonReview;
import com.example.finalproject.Logic.Notification;
import com.example.finalproject.Logic.School;
import com.example.finalproject.Logic.Student;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.Logic.User;
import com.example.finalproject.interfaces.OnCompleteFeedbacksTask;
import com.example.finalproject.session.SessionUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FirebaseDatabaseManager {

    private static final String SUBJECTS = "subjects";
    private static final String TYPES = "types";
    private static final String LESSONS = "lessons";
    private static final String TEACHERS = "teachers";
    private static final String STUDENTS = "students";
    private static final String SCHOOLS = "schools";
    private static final String REVIEWS = "reviews";
    private static final String LESSONS_REQUESTS = "lessons_requests";
    private static final String FEEDBACKS = "feedbacks";
    private static final String NOTIFICATIONS = "notifications";






    public static void getSubjects(OnSuccessListener<DocumentSnapshot> onSuccessListener) {
        FirebaseFirestore.getInstance().collection(SUBJECTS)
                .document(TYPES)
                .get()
                .addOnSuccessListener(onSuccessListener);
    }

    public static void getSchoolLessonsByLevel(int level, OnSuccessListener<QuerySnapshot> onSuccessListener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .whereEqualTo("level", level)
                .get()
                .addOnSuccessListener(onSuccessListener);
    }

    public static void getUserLessons(String userId, boolean isTeacher, OnCompleteListener<DocumentSnapshot> listener) {
        FirebaseFirestore.getInstance().collection(isTeacher ? TEACHERS : STUDENTS)
                .document(userId)
                .get()
                .addOnCompleteListener(listener);
    }

    public static void getAllSchools(OnCompleteListener<QuerySnapshot> listener) {
        FirebaseFirestore.getInstance().collection(SCHOOLS)
                .get()
                .addOnCompleteListener(listener);
    }

    public static void addLessonByTeacher(Lesson lesson, OnCompleteListener listener) {
        SessionUser.currentUser.getLessons().put(lesson.getId(), lesson.copy());
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .set(lesson)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            setUser(SessionUser.currentUser, listener);
                        }
                    }
                });
    }

    public static void registerToLesson(Lesson lesson, Student student, OnCompleteListener<Void> listener) {
        lesson.getStudents().put(student.getId(), student);
        lesson.setTeacher(lesson.getTeacher().copy());
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .set(lesson)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            SessionUser.currentUser.getLessons().put(lesson.getId(), lesson.copy());
                            setUser(SessionUser.currentUser, listener);
                            updateTeacherLessons(lesson, student);
                        }
                    }
                });
    }

    private static void updateTeacherLessons(Lesson lesson, Student student) {
        FirebaseFirestore.getInstance().collection(TEACHERS)
                .document(lesson.getTeacher().getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Teacher teacher = documentSnapshot.toObject(Teacher.class);
                        Lesson teacherLesson = teacher.getLessons().get(lesson.getId());
                        teacherLesson.getStudents().put(student.getId(), student);
                        teacherLesson.setTeacher(null);
                        saveUser(teacher, null);
                    }
                });
    }

    public static void saveUser(User user, OnCompleteListener<Void> listener) {
        FirebaseFirestore.getInstance().collection(SessionUser.isTeacher() ? TEACHERS : STUDENTS)
                .document(user.getId())
                .set(user)
                .addOnCompleteListener(listener);
    }

    public static void saveStudent(User student, OnCompleteListener<Void> listener) {
        FirebaseFirestore.getInstance().collection(TEACHERS)
                .document(student.getId())
                .set(student)
                .addOnCompleteListener(listener);
    }

    public static void getCurrentUser(OnCompleteListener<DocumentSnapshot> listener) {
        FirebaseFirestore.getInstance().collection(SessionUser.isTeacher() ? TEACHERS : STUDENTS)
                .document(SessionUser.getUserId())
                .get()
                .addOnCompleteListener(listener);
    }

    public static void getUser(OnCompleteListener<DocumentSnapshot> listener) {
        FirebaseFirestore.getInstance().collection(SessionUser.isTeacher() ? TEACHERS : STUDENTS)
                .document(SessionUser.getUserId())
                .get()
                .addOnCompleteListener(listener);
    }

    public static void setUser(User user, OnCompleteListener listener) {
        FirebaseFirestore.getInstance().collection(user instanceof Teacher ? TEACHERS : STUDENTS)
                .document(user.getId())
                .set(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (listener != null) {
                            listener.onComplete(task);
                        }
                    }
                });
    }

    public static void updateUser(User user, OnCompleteListener listener) {
        FirebaseFirestore.getInstance().collection(user instanceof Teacher ? TEACHERS : STUDENTS)
                .document(user.getId())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            User user1 = task.getResult().toObject(User.class);
                            for (Lesson lesson : user1.getLessons().values()) {
                                if (user instanceof Teacher) {
                                    lesson.setTeacher(((Teacher) user).copy());
                                    updateLessonTeacher(lesson);
                                } else {
                                    Student copyStudent = ((Student) user).copy();
                                    lesson.getStudents().put(user.getId(), copyStudent);
                                    updateLessonStudents(lesson, copyStudent);
                                }
                            }
                            updateUserFields(user, listener);
                        } else {
                            listener.onComplete(task);
                        }
                    }
                });
    }

    private static void updateLessonReview(LessonReview review , Student copyStudent) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                            Lesson lesson = snapshot.toObject(Lesson.class);

                        }
                    }
                });

    }

    private static void updateUserFields(User user, OnCompleteListener listener) {
        FirebaseFirestore.getInstance().collection(user instanceof Teacher ? TEACHERS : STUDENTS)
                .document(user.getId())
                .update("phone", user.getPhone(),
                        "imageUrl", user.getImageUrl(),
                        "age", user.getAge(),
                        "region", user.getRegion())
                .addOnCompleteListener(listener);
    }

    private static void updateLessonTeacher(Lesson lesson) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .update("teacher", lesson.getTeacher());

    }

    private static void updateLessonStudents(Lesson lesson, Student student) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Lesson lesson1 = documentSnapshot.toObject(Lesson.class);
                        lesson1.getStudents().put(student.getId(), student);
                        LessonReview lessonReview = lesson1.getLessonReviews().getOrDefault(student.getId(), null);
                        if (lessonReview != null) {
                            lessonReview.setReviewer(student);
                        }
                        FirebaseFirestore.getInstance().collection(LESSONS)
                                .document(lesson.getId())
                                .update("students", lesson1.getStudents(),
                                        "lessonReviews", lesson1.getLessonReviews());
                    }
                });
    }


    private static void updateClassesTeacher(User user, OnCompleteListener<QuerySnapshot> listener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Lesson> updatedLesson = new ArrayList<>();
                            List<DocumentSnapshot> documents = task.getResult().getDocuments();
                            for (DocumentSnapshot snapshot : documents) {
                                Lesson lesson = snapshot.toObject(Lesson.class);
                                if (lesson.getTeacher().getId().equals(user.getId())) {
                                    updatedLesson.add(lesson);
                                }
                            }

                            for (Lesson lesson : updatedLesson) {
                                FirebaseFirestore.getInstance().collection(LESSONS)
                                        .document(lesson.getId())
                                        .update("teacher", user);
                            }
                            if (listener != null) {
                                listener.onComplete(task);
                            }
                        }
                    }
                });
    }

    public static void getTeacher(String userId, OnCompleteListener<DocumentSnapshot> listener, boolean registerForUpdate) {
        FirebaseFirestore.getInstance().collection(TEACHERS)
                .document(userId).get().addOnCompleteListener(listener);
        if (registerForUpdate) {
            registerForUpdates(TEACHERS, userId, true);
        }
    }

    public static void getStudent(String userId, OnCompleteListener<DocumentSnapshot> listener) {
        FirebaseFirestore.getInstance().collection(STUDENTS)
                .document(userId).get().addOnCompleteListener(listener);
        registerForUpdates(STUDENTS, userId, false);
    }

    private static void registerForUpdates(String collection, String uid, boolean isTeacher) {
        FirebaseFirestore.getInstance().collection(collection).document(uid).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException error) {
                if (snapshot != null && snapshot.exists()) {
                    if (isTeacher) {
                        SessionUser.currentUser = snapshot.toObject(Teacher.class);
                    } else {
                        SessionUser.currentUser = snapshot.toObject(Student.class);
                    }
                }
            }
        });
    }
    public static void removeLesson(Lesson lesson, OnCompleteListener listener) {
        if (!SessionUser.isStudent()) {
            removeLessonAtAll(lesson, listener);
        } else {
            removeLessonByStudent(lesson, listener);
        }

    }

    private static void removeLessonByStudent(Lesson lesson, OnCompleteListener listener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .update("students", lesson.getStudents())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            setUser(SessionUser.currentUser, listener);
                        }
                    }
                });
    }

    private static void removeLessonAtAll(Lesson lesson, OnCompleteListener listener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        FirebaseDatabaseManager.setUser(SessionUser.currentUser, null);
                        Lesson removed = documentSnapshot.toObject(Lesson.class);
                        FirebaseFirestore.getInstance().collection(LESSONS).document(lesson.getId()).delete();
                        for (Student student : removed.getStudents().values()) {
                            FirebaseFirestore.getInstance().collection(STUDENTS)
                                    .document(student.getId())
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                Student student1 = task.getResult().toObject(Student.class);
                                                student1.getLessons().remove(removed.getId());
                                                FirebaseDatabaseManager.setUser(student1, null);
                                            }
                                            listener.onComplete(task);
                                        }
                                    });
                        }
                    }
                });

    }

    public static void getAllLessons(OnCompleteListener<QuerySnapshot> listener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .get()
                .addOnCompleteListener(listener);

    }

    public static void getUsers(boolean getTeachers, List<User> users, OnSuccessListener<Void> listener) {
        FirebaseFirestore.getInstance().collection(getTeachers ? TEACHERS : STUDENTS)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                            users.add(snapshot.toObject(User.class));
                        }
                        listener.onSuccess(null);
                    }
                });
    }

    public static void setLesson(Lesson lesson, OnCompleteListener<Void> listener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .set(lesson)
                .addOnCompleteListener(listener);
    }

    public static void updateLesson(Lesson lesson, OnCompleteListener<Void> listener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        Lesson lesson1 = documentSnapshot.toObject(Lesson.class);
                        lesson1.setBriefInformation(lesson.getBriefInformation());
                        lesson1.setSubject(lesson.getSubject());
                        lesson1.setLevel(lesson.getLevel());
                        lesson1.setSchool(lesson.getSchool());
                        lesson1.setFile(lesson.getFile());
                        lesson1.setPrice(lesson.getPrice());
                        lesson1.setStudentsLimit(lesson.getStudentsLimit());
                        setLesson(lesson1,null);
                        updateLessonToTeacherAndStudent(lesson1, listener);

                    }
                });
    }

    private static void updateLessonToTeacherAndStudent(Lesson lesson, OnCompleteListener listener) {
        FirebaseFirestore.getInstance().collection(TEACHERS)
                .document(lesson.getTeacher().getId())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            Teacher teacher = task.getResult().toObject(Teacher.class);
                            teacher.getLessons().put(lesson.getId(), lesson);
                            setUser(teacher, null);
                            if (listener != null) {
                                listener.onComplete(task);
                            }
                        }
                    }
                });
        for (Student student : lesson.getStudents().values()) {
            FirebaseFirestore.getInstance().collection(STUDENTS)
                    .document(student.getId())
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            Student student1 = documentSnapshot.toObject(Student.class);
                            student1.getLessons().put(lesson.getId(), lesson);
                            setUser(student1, null);
                        }
                    });
        }
    }

    public static void getLessonById(String id, OnCompleteListener<DocumentSnapshot> listener) {
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(id)
                .get()
                .addOnCompleteListener(listener);
    }

    public static void saveSchool(School school,  OnCompleteListener<Void> listener) {
        FirebaseFirestore.getInstance().collection(SCHOOLS)
                .document(school.getId())
                .set(school)
                .addOnCompleteListener(listener);
    }

    public static void addReview(LessonReview lessonReview, Lesson lesson, OnCompleteListener<Void> listener) {
        FirebaseDatabaseManager.setLesson(lesson, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                FirebaseFirestore.getInstance().collection(REVIEWS)
                        .document(lessonReview.getId())
                        .set(lessonReview)
                        .addOnCompleteListener(listener);
            }
        });
    }

    public static void removeReview(Lesson lesson, OnSuccessListener listener) {
        lesson.getLessonReviews().remove(SessionUser.currentUser.getId());
        FirebaseFirestore.getInstance().collection(LESSONS)
                .document(lesson.getId())
                .update("lessonReviews", lesson.getLessonReviews())
                .addOnSuccessListener(listener);
    }

    public static void registerToFutureLesson(LessonRequest lessonRequest, OnCompleteListener<Void> listener) {
        FirebaseFirestore.getInstance().collection(LESSONS_REQUESTS)
                .document(lessonRequest.getId())
                .set(lessonRequest)
                .addOnCompleteListener(listener);
    }

    public static void getAllLessonRequests(Lesson lesson, OnCompleteListener<QuerySnapshot> listener) {
        FirebaseFirestore.getInstance().collection(LESSONS_REQUESTS)
                .whereEqualTo("subject", lesson.getSubject())
                .whereEqualTo("level", lesson.getLevel())
                .get()
                .addOnCompleteListener(listener);
    }

    public static void addFeedback(Feedback feedback, OnCompleteListener listener) {
        FirebaseFirestore.getInstance().collection(FEEDBACKS)
                .document(feedback.getId())
                .set(feedback)
                .addOnCompleteListener(listener);
    }

    public static void getFeedbackByLesson(String lessonId, String studentId, OnCompleteFeedbacksTask listener) {
        FirebaseFirestore.getInstance().collection(FEEDBACKS)
                .whereEqualTo("lessonId", lessonId)
                .whereEqualTo("studentId", studentId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Feedback> feedbacks = new ArrayList<>();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            Feedback feedback = documentSnapshot.toObject(Feedback.class);
                            feedbacks.add(feedback);
                        }
                        if (feedbacks.isEmpty()) {
                            listener.OnCompleteFeedbacks(feedbacks);
                        } else {
                            int i;
                            for (i = 0; i < feedbacks.size() ; i++) {
                                int finalI = i;
                                FirebaseFirestore.getInstance().collection(TEACHERS).document(feedbacks.get(i).getTeacherId()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                    @Override
                                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                                        feedbacks.get(finalI).setTeacher(documentSnapshot.toObject(Teacher.class));
                                        if (finalI == feedbacks.size() - 1) {
                                            listener.OnCompleteFeedbacks(feedbacks);
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
    }

    public static void saveNotification(String title, String body) {
        Notification notification = new Notification(UUID.randomUUID().toString(), title, body, System.currentTimeMillis(), FirebaseAuth.getInstance().getUid());
        FirebaseFirestore.getInstance().collection(NOTIFICATIONS)
                .document(String.valueOf(System.currentTimeMillis()))
                .set(notification);
    }

    public static void getAllNotifications(OnCompleteListener<QuerySnapshot> listener) {
        FirebaseFirestore.getInstance().collection(NOTIFICATIONS)
                .whereEqualTo("userId", SessionUser.getUserId())
                .get()
                .addOnCompleteListener(listener);
    }

    public static void removeNotification(Notification notification, OnSuccessListener listener) {
        FirebaseFirestore.getInstance().collection(NOTIFICATIONS)
                .document(notification.getId())
                .delete()
                .addOnSuccessListener(listener);
    }

    public static void getFeedbackByUserId(String userId, OnCompleteFeedbacksTask listener) {
        FirebaseFirestore.getInstance().collection(FEEDBACKS)
                .whereEqualTo("teacherId", userId)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        List<Feedback> feedbacks = new ArrayList<>();
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                            Feedback feedback = documentSnapshot.toObject(Feedback.class);
                            feedback.setTeacher((Teacher) SessionUser.currentUser);
                            feedbacks.add(feedback);
                        }
                        listener.OnCompleteFeedbacks(feedbacks);
                    }
                });
    }
}
