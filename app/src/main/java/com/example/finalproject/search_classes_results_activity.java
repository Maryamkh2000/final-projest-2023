package com.example.finalproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.finalproject.Logic.Lesson;

public class search_classes_results_activity extends AppCompatActivity {

    private ImageView arrow6;
    private RecyclerView classesResultsRv;
    private TextView noResultFoundTv;
    private ArrayList<Lesson> lessons = new ArrayList<Lesson>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_classes_results);

        arrow6 = findViewById(R.id.arrow6);
        classesResultsRv = findViewById(R.id.classesResultsRv);
        noResultFoundTv = findViewById(R.id.noResultFoundTv);

//        lessons = getIntent().getParcelableArrayListExtra("classes");
        System.out.println(lessons);

        classesResultsRv.setLayoutManager(new LinearLayoutManager(this));
        classesResultsRv.setAdapter(new ResultClassesAdapter(getApplicationContext(), lessons));

        if(lessons.isEmpty()){
            classesResultsRv.setVisibility(View.INVISIBLE);
            noResultFoundTv.setVisibility(View.VISIBLE);
        }
        else{
            classesResultsRv.setVisibility(View.VISIBLE);
            noResultFoundTv.setVisibility(View.INVISIBLE);
        }

        arrow6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(search_classes_results_activity.this, search_classes.class);
                startActivity(intent);
            }
        });

    }
}