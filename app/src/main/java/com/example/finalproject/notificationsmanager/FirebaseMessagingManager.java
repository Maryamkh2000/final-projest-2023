package com.example.finalproject.notificationsmanager;

import android.content.Context;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FirebaseMessagingManager {

    private static final String URL = "https://fcm.googleapis.com/fcm/send";
    private static final String SERVER_KEY = "AAAA22rJMqg:APA91bEl8MoUgFA-REtHxcnWDwoATdLx4Dux3jIcS_EkZvu7gBfp40wHn5lXZzLL4obWAY8MfPg_8OmMN0EwjVXunrGlo0kEeFNrCb38gbmnXVewvYBhDSy0Cyw_VHNdPpHr7m3YUlC5";
    public static final String LESSON_TEACHER_SUFIX = "teacher";


    public static void unSubscribePush() {
        FirebaseMessaging.getInstance().deleteToken();
    }

    public static void subscribeToPush(String id) {
        FirebaseMessaging.getInstance().subscribeToTopic(id).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
            }
        });
    }

    public static void sendNotification(Context context , String id, String title, String body) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JSONObject mainObject = new JSONObject();
        try {
            mainObject.put("to", "/topics/" + id);
            JSONObject object = new JSONObject();
            object.put("title", title);
            object.put("body", body);
            mainObject.put("data", object);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,
                mainObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                int x = 0;
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                int x = 0;
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String , String > header = new HashMap<>();
                header.put("content-type", "application/json");
                header.put("authorization", "key=" + SERVER_KEY);
                return header;
            }
        };
        requestQueue.add(request);
    }
    public static void sendNotification(Context context , String id, String title) {
        sendNotification(context, id, title, null);
    }
}
