package com.example.finalproject.interfaces;

public interface HasNext {
    void goNext();
}
