package com.example.finalproject.interfaces;

import com.example.finalproject.Logic.Feedback;

import java.util.List;

public interface OnCompleteFeedbacksTask {

    void OnCompleteFeedbacks(List<Feedback> feedbacks);
}
