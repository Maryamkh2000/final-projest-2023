package com.example.finalproject.interfaces;

public interface OnClickListener {
    void onItemClicked(int position);
}
