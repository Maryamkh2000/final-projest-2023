package com.example.finalproject.interfaces;

public interface HasBack {
    void handlePrevButton();
}
