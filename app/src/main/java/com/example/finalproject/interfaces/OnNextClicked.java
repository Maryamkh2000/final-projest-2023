package com.example.finalproject.interfaces;

import com.example.finalproject.Logic.Lesson;

public interface OnNextClicked {
    void onNextClicked(Lesson lesson);
}
