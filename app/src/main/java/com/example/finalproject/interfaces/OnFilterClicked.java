package com.example.finalproject.interfaces;

import com.example.finalproject.Logic.CheckedFilterItem;

public interface OnFilterClicked {

    void onFilterClicked(CheckedFilterItem checkedFilterItem);
}
