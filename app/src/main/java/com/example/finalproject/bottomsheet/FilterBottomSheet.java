package com.example.finalproject.bottomsheet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.CheckedFilterItem;
import com.example.finalproject.R;
import com.example.finalproject.adapters.FiltersAdapter;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.interfaces.OnFilterClicked;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class FilterBottomSheet extends BottomSheetDialogFragment {

    private TextView titleTV;
    private RecyclerView list;
    private FiltersAdapter adapter;
    private List<CheckedFilterItem> items = new ArrayList<>();
    private OnFilterClicked listener;
    private String title;

    public FilterBottomSheet(List<CheckedFilterItem> filterItems, String title, OnFilterClicked clickListener) {
        items = filterItems;
        listener = clickListener;
        this.title = title;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleTV = view.findViewById(R.id.title);
        titleTV.setText(title);
        list = view.findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        FiltersAdapter filtersAdapter = new FiltersAdapter(view.getContext(), items, new OnFilterClicked() {
            @Override
            public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                items.forEach(new Consumer<CheckedFilterItem>() {
                    @Override
                    public void accept(CheckedFilterItem checkedFilterItem) {
                        checkedFilterItem.setChecked(false);
                    }
                });
                checkedFilterItem.setChecked(true);
                listener.onFilterClicked(checkedFilterItem);
                FilterBottomSheet.this.dismiss();
            }
        });
        list.setAdapter(filtersAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottom_sheet, container, false);
    }
}
