package com.example.finalproject.session;

import com.example.finalproject.Logic.User;
import com.google.firebase.auth.FirebaseAuth;

public class SessionUser {

    public static User currentUser;
    public static boolean isTeacher() {
        return "Teacher".equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
    }
    public static boolean isStudent() {
        return "Student".equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
    }

    public static String getUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public static boolean isAdmin() {
        return "admin".equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
    }
}
