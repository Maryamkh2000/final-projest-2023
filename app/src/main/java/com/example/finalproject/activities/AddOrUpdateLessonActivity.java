package com.example.finalproject.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Enums.Step;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.LessonRequest;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.broadcastreceiver.MyBroadcastReceiver;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.fragments.MoreLessonDetailsFragment;
import com.example.finalproject.fragments.SelectLevelsFragment;
import com.example.finalproject.fragments.SelectSchoolFragment;
import com.example.finalproject.fragments.SelectSubjectFragment;
import com.example.finalproject.fragments.SelectTimeFragment;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddOrUpdateLessonActivity extends AppCompatActivity implements HasNext, HasBack {

    public static final String LESSON = "LESSON";
    public static final String IS_UPDATE = "IS_UPDATE";

    private Lesson lesson = new Lesson();
    private Step currentStep = Step.Subjects;
    private Button prevBt;
    private boolean isUpdateFlow;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        ImageView close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Button nextBt = findViewById(R.id.next);
        nextBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((OnNextClicked)getcurrentFragment()).onNextClicked(lesson);
            }
        });
        prevBt = findViewById(R.id.prev);
        prevBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().popBackStack();
            }
        });
        lesson = (Lesson) getIntent().getSerializableExtra(LESSON);
        isUpdateFlow = getIntent().getBooleanExtra(IS_UPDATE, false);
        if (lesson == null) {
            lesson = new Lesson();
        }
        startFlow();
    }

    private void startFlow() {
        SelectSubjectFragment fragment =  new SelectSubjectFragment();
        Bundle arg = new Bundle();
        arg.putString(SelectSubjectFragment.SELECTED_SUBJECT, lesson.getSubject());
        fragment.setArguments(arg);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    private Fragment getcurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.container);
    }

    @Override
    public void goNext() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        Fragment destination = null;
        if (fragment instanceof SelectSubjectFragment){
            destination = new SelectLevelsFragment();
            Bundle arg = new Bundle();
            arg.putInt(SelectLevelsFragment.SELECTED_LEVEL, lesson.getLevel());
            destination.setArguments(arg);
        } else if (fragment instanceof  SelectLevelsFragment) {
            destination = new SelectSchoolFragment();
            Bundle arg = new Bundle();
            arg.putSerializable(SelectSchoolFragment.SELECTED_SCHOOL, lesson.getSchool());
            destination.setArguments(arg);
        } else if (fragment instanceof  SelectSchoolFragment) {
            Bundle arg = new Bundle();
            arg.putSerializable("school", lesson.getSchool());
            destination = new SelectTimeFragment();
            destination.setArguments(arg);
        } else if (fragment instanceof SelectTimeFragment) {
            destination = new MoreLessonDetailsFragment();
            Bundle arg = new Bundle();
            arg.putString(MoreLessonDetailsFragment.BRIEF, lesson.getBriefInformation());
            arg.putInt(MoreLessonDetailsFragment.DURATION, lesson.getDuration());
            arg.putString(MoreLessonDetailsFragment.PRICE, String.valueOf(lesson.getPrice()));
            arg.putString(MoreLessonDetailsFragment.MAX, String.valueOf(lesson.getStudentsLimit()));
            destination.setArguments(arg);
        } else {
            if (isUpdateFlow) {
                SessionUser.currentUser.getLessons().put(lesson.getId(), lesson);
                updatedLesson();
            } else {
                addNewLesson();
            }
            return;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, destination)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    private void updatedLesson() {
        FirebaseDatabaseManager.updateLesson(lesson , new OnCompleteListener<Void>(){
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(AddOrUpdateLessonActivity.this,
                            getString(isUpdateFlow ? R.string.lesson_is_updated_successfully : R.string.leasson_added_successfully), Toast.LENGTH_LONG).show();
                    onDone();
                } else {
                    AlertDialogUtils.showAlertDialog(getApplicationContext(), getString(R.string.error), task.getException().getMessage());
                }
            }
        });
    }

    private void onDone() {
        notifyLessonRequests();
        notifyLessonStart();
        FirebaseMessagingManager.subscribeToPush(SessionUser.getUserId());
        finish();
    }

    private void notifyLessonStart() {
        long time = lesson.getTime() - TimeUnit.MINUTES.toMillis(9);
        Intent intent = new Intent(this, MyBroadcastReceiver.class);
        intent.putExtra("id", lesson.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 0, intent, PendingIntent.FLAG_IMMUTABLE);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
    }

    private void notifyLessonRequests() {
        FirebaseDatabaseManager.getAllLessonRequests(lesson, new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<DocumentSnapshot> snapshots = task.getResult().getDocuments();
                    for (DocumentSnapshot snapshot : snapshots) {
                        LessonRequest lessonRequest = snapshot.toObject(LessonRequest.class);
                        if (lessonRequest.getSchool().equals(lesson.getSchool())) {
                            FirebaseMessagingManager.sendNotification(
                                    AddOrUpdateLessonActivity.this, lessonRequest.getId(),
                                    getString(R.string.a_new_lessons_has_been_created_that_may_interest_you));
                        }
                    }
                }
            }
        });
    }
    private void addNewLesson() {
        lesson.setTeacher(((Teacher) SessionUser.currentUser).copy());
        lesson.setId(String.valueOf(System.currentTimeMillis()));
        FirebaseDatabaseManager.addLessonByTeacher(lesson, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(AddOrUpdateLessonActivity.this, getString(R.string.leasson_added_successfully), Toast.LENGTH_LONG).show();
                    onDone();
                } else {
                    AlertDialogUtils.showAlertDialog(getApplicationContext(), getString(R.string.error), task.getException().getMessage());
                }
            }
        });
    }

    public void handlePrevButton() {
        prevBt.setEnabled(!(getcurrentFragment() instanceof SelectSubjectFragment));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
