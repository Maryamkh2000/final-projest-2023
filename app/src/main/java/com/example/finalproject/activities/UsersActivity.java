package com.example.finalproject.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.User;
import com.example.finalproject.R;
import com.example.finalproject.adapters.UsersAdapter;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

public class UsersActivity extends ListActivity {

    public static final String TEACHERS  = "TEACHERS";
    public static final String LESSON  = "LESSON";


    private UsersAdapter adapter;
    private ArrayList<User> users = new ArrayList<User>();
    private boolean fetchTeachers = false;
    private Lesson lesson;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        fetchTeachers = getIntent().getBooleanExtra(TEACHERS, false);
        super.onCreate(savedInstanceState);
        lesson =  (Lesson) getIntent().getSerializableExtra(LESSON);
        fetchData();
        adapter = new UsersAdapter(this, users, new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Intent intent = new Intent(UsersActivity.this, CreateFeedbackForStudentActivity.class);
                intent.putExtra(CreateFeedbackForStudentActivity.TEACHER_ID, SessionUser.getUserId());
                intent.putExtra(CreateFeedbackForStudentActivity.STUDENT_ID, users.get(position).getId());
                intent.putExtra(CreateFeedbackForStudentActivity.LESSON_ID, lesson.getId());
                startActivity(intent);
            }
        }, new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + users.get(position).getEmail()));
                startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
            }
        });
        list.setAdapter(adapter);
    }

    private void fetchData() {
        swipeRefreshLayout.setRefreshing(true);
        if (lesson == null) {
            fetchAllUsers();
        } else {
            fetchUsersByLessonId();
        }
    }
    private void fetchUsersByLessonId() {
        FirebaseDatabaseManager.getLessonById(lesson.getId(), new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        swipeRefreshLayout.setRefreshing(false);
                        users.clear();
                        if (task.isSuccessful()) {
                            Lesson lesson1 = task.getResult().toObject(Lesson.class);
                            users.addAll(lesson1.getStudents().values());
                            if (users.isEmpty()) {
                                AlertDialogUtils.showAlertDialog(UsersActivity.this, getString(R.string.alert)
                                        , getString(R.string.no_students));
                            } else {
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    private void fetchAllUsers() {
        users.clear();
        FirebaseDatabaseManager.getUsers(fetchTeachers, users, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                swipeRefreshLayout.setRefreshing(false);
                if (users.isEmpty()) {
                    AlertDialogUtils.showAlertDialog(UsersActivity.this, getString(R.string.alert)
                    , getString(R.string.no_students));
                } else{
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    protected String getToolbarTitle() {
        return getString(fetchTeachers ? R.string.all_teachers : R.string.all_students);
    }

    @Override
    protected void reload() {
        super.reload();
        fetchData();
    }
}
