package com.example.finalproject.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.finalproject.Logic.Student;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginActivity extends Activity {


	private static final String TAG = "log in activity";


	private Button log_in;
	private ImageView vector;
	private TextView forgotten_password__;
	private TextView don_t_have_an_accoount__sign_up;
	private ImageView arrow_left_by_streamlinehq_ek1;
	private EditText editTextTextEmailAddress;
	private EditText editTextTextPassword;
	private TextView textView;
	private TextView textView2;
	private FirebaseAuth auth;
	FirebaseFirestore db = FirebaseFirestore.getInstance();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		auth = FirebaseAuth.getInstance();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		log_in = (Button) findViewById(R.id.send);
		vector = (ImageView) findViewById(R.id.vector);
		forgotten_password__ = (TextView) findViewById(R.id.forgotten_password__);
		don_t_have_an_accoount__sign_up = (TextView) findViewById(R.id.don_t_have_an_accoount__sign_up);
		arrow_left_by_streamlinehq_ek1 = (ImageView) findViewById(R.id.arrow_left_by_streamlinehq_ek1);
		editTextTextPassword = (EditText) findViewById(R.id.editTextTextPassword);
		editTextTextEmailAddress = (EditText) findViewById(R.id.editTextTextEmailAddress);
		textView = (TextView) findViewById(R.id.textView);
		textView2 = (TextView) findViewById(R.id.textView2);


		arrow_left_by_streamlinehq_ek1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		don_t_have_an_accoount__sign_up.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
				startActivity(intent);
				finish();
			}
		});
		log_in.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int check = 1;

				String email = editTextTextEmailAddress.getText().toString().trim();
				String password = editTextTextPassword.getText().toString().trim();

				if (email.equals("")) {
					textView.setVisibility(View.VISIBLE);
					check = 0;
				}
				if (password.equals("")) {
					textView2.setVisibility(View.VISIBLE);
					check = 0;
				}

				if (check == 1) {
					auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if (task.isSuccessful()) {
								goToRelevantScreen();
							} else {
								AlertDialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.error), task.getException().getMessage());
							}
						}
					});
				}
			}
		});

		vector.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (editTextTextPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
					editTextTextPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
				} else {
					editTextTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
				}
			}
		});
		forgotten_password__.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
				startActivity(intent);
			}
		});
	}

	//TODO
	private String getId() {
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String id;
						if (task.isSuccessful()) {
							id = task.getResult().getDocuments().get(0).get("id").toString();
						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							id = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("id").toString();
		return result;
	}

	private String getAge() {
		String result = null;


		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String age;
						if (task.isSuccessful()) {
							age = task.getResult().getDocuments().get(0).get("age").toString();
						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							age = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("age").toString();
		return result;
	}

	private String getGender() {
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String gender;
						if (task.isSuccessful()) {

							gender = task.getResult().getDocuments().get(0).get("gender").toString();

						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							gender = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("gender").toString();
		return result;

	}

	private String getImage() {
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String image;
						if (task.isSuccessful()) {
							image = task.getResult().getDocuments().get(0).get("image").toString();
						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							image = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("image").toString();
		return result;

	}

	private String getType() {
		String result = null;
		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String type;
						if (task.isSuccessful()) {
							type = task.getResult().getDocuments().get(0).get("type").toString();
						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							type = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("type").toString();
		return result;

	}

	private String getRegion() {
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String region;
						if (task.isSuccessful()) {

							region = task.getResult().getDocuments().get(0).get("region").toString();


						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							region = null;
						}

					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("region").toString();
		return result;

	}

	private String getPassword() {
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String password;
						if (task.isSuccessful()) {
							password = task.getResult().getDocuments().get(0).get("password").toString();
						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							password = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("password").toString();
		return result;

	}

	private String getPhone() {
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String phone;
						if (task.isSuccessful()) {

							phone = task.getResult().getDocuments().get(0).get("phone").toString();

						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							phone = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("phone").toString();
		return result;

	}

	private String getEmail() {
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String email;
						if (task.isSuccessful()) {
							email = task.getResult().getDocuments().get(0).get("email").toString();
						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							email = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("email").toString();
		return result;

	}

	private String getUserName() {

		System.out.println("StudentHomePageActivity.id");
		String result = null;

		Task<QuerySnapshot> query = db.collection("Users")
				.whereEqualTo("email", editTextTextEmailAddress.getText().toString())
				.get()
				.addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
					@Override
					public void onComplete(@NonNull Task<QuerySnapshot> task) {
						String userName;
						if (task.isSuccessful()) {
							userName = task.getResult().getDocuments().get(0).get("userName").toString();
						} else {
							Log.d(TAG, "Error getting documents: ", task.getException());
							userName = null;
						}
					}
				});
		while (!query.isComplete()) {

		}
		result = query.getResult().getDocuments().get(0).get("userName").toString();
		return result;
	}


	private void goToRelevantScreen() {
		FirebaseDatabaseManager.getCurrentUser(new OnCompleteListener<DocumentSnapshot>() {
			@Override
			public void onComplete(@NonNull Task<DocumentSnapshot> task) {
				Class destination;
				if (task.isSuccessful()) {
					if ("Teacher".equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())) {
						destination = TeacherHomePageActivity.class;
						SessionUser.currentUser = task.getResult().toObject(Teacher.class);
					} else if ("Student".equals(FirebaseAuth.getInstance().getCurrentUser().getDisplayName())){
						SessionUser.currentUser = task.getResult().toObject(Student.class);
						destination = StudentHomePageActivity.class;
					} else {
						destination = AdminHomePageActivity.class;
					}

					Intent intent = new Intent(LoginActivity.this, destination);
					Toast.makeText(getApplicationContext(), "Login succeed.", Toast.LENGTH_SHORT).show();
					startActivity(intent);
					finish();
				}

			}
		});
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}