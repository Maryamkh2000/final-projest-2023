package com.example.finalproject.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.School;
import com.example.finalproject.Logic.Student;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.fragments.PaymentFragment;
import com.example.finalproject.fragments.SelectDateFragment;
import com.example.finalproject.fragments.SelectLevelsFragment;
import com.example.finalproject.fragments.SelectSchoolFragment;
import com.example.finalproject.fragments.SelectSubjectFragment;
import com.example.finalproject.fragments.SelectTeacherFragment;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class RegisterToLessonActivity extends AppCompatActivity implements HasNext, HasBack {

    private Lesson lesson = new Lesson();
    private List<Lesson> allLessons = new ArrayList<>();
    private Button prevBt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        ImageView close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Button nextBt = findViewById(R.id.next);
        nextBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((OnNextClicked) getCurrentFragment()).onNextClicked(lesson);
            }
        });
        prevBt = findViewById(R.id.prev);
        prevBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().popBackStack();
            }
        });
        FirebaseDatabaseManager.getAllLessons(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<DocumentSnapshot> documentSnapshots = task.getResult().getDocuments();
                    for (DocumentSnapshot snapshot : documentSnapshots) {
                        allLessons.add(snapshot.toObject(Lesson.class));
                    }

                    Iterator<Lesson> iterator = allLessons.iterator();
                    while (iterator.hasNext()) {
                        Lesson current = iterator.next();
                        if (current.getStudentsLimit() == current.getStudents().size() || SessionUser.currentUser.getLessons().containsKey(current.getId())) {
                            iterator.remove();
                        }
                    }
                    SelectSubjectFragment fragment = new SelectSubjectFragment();
                    Bundle arg = new Bundle();
                    arg.putSerializable("subjects", getRelevantFilter());
                    fragment.setArguments(arg);
                    if (allLessons.size() == 0) {
                        AlertDialogUtils.showAlertOkDialog(RegisterToLessonActivity.this, getString(R.string.alert), getString(R.string.no_available_lessons), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        });
                    }else {
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, fragment)
                                .addToBackStack(fragment.getClass().getSimpleName())
                                .commit();
                    }

                }
            }
        });
    }

    private Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.container);
    }

    @Override
    public void goNext() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        Fragment destination = null;
        if (fragment instanceof SelectSubjectFragment) {
            destination = new SelectLevelsFragment();
            Bundle arg = new Bundle();
            ArrayList<Integer> list = getFilterLevelsBySubject();
            if (list.isEmpty()) {
                showError();
                return;
            }
            arg.putSerializable("levels", list);
            destination.setArguments(arg);
        } else if (fragment instanceof SelectLevelsFragment) {
            destination = new SelectSchoolFragment();
            Bundle arg = new Bundle();
            arg.putSerializable("schools", getFilterSchools());
            destination.setArguments(arg);
        } else if (fragment instanceof SelectSchoolFragment) {
            destination = new SelectTeacherFragment();
            Bundle arg = new Bundle();
            arg.putSerializable("teachers", getFilterTeachers());
            destination.setArguments(arg);
        } else if (fragment instanceof SelectTeacherFragment) {
            destination = new SelectDateFragment();
            Bundle arg = new Bundle();
            arg.putSerializable("dates", getFilterDates());
            destination.setArguments(arg);
        } else if (fragment instanceof SelectDateFragment) {
            destination = new PaymentFragment();
        } else {
            Lesson relevantLesson = getSelectedLesson();
            Student student = ((Student) SessionUser.currentUser).copy();
            FirebaseDatabaseManager.registerToLesson(relevantLesson,student,  new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                       onDone(relevantLesson);
                    } else {
                        AlertDialogUtils.showAlertDialog(getApplicationContext(), getString(R.string.error), task.getException().getMessage());
                    }
                }
            });
            return;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, destination)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    private void onDone(Lesson lesson) {
        Toast.makeText(RegisterToLessonActivity.this, getString(R.string.leasson_added_successfully), Toast.LENGTH_LONG).show();
        FirebaseMessagingManager.subscribeToPush(lesson.getId());
        FirebaseMessagingManager.sendNotification(RegisterToLessonActivity.this,
                lesson.getTeacher().getId(), getString(R.string.new_register, SessionUser.currentUser.getName()));
        finish();
    }

    private void showError() {
        AlertDialogUtils.showAlertDialog(this, getString(R.string.no_result), getString(R.string.there_are_no_lessons_in_this_subject));
    }

    private Lesson getSelectedLesson() {
        for (Lesson lesson : allLessons) {
            if (lesson.getSubject().equals(this.lesson.getSubject())
                    && lesson.getLevel() == this.lesson.getLevel()
                    && lesson.getSchool().equals(this.lesson.getSchool())
                    && lesson.getTeacher().equals(this.lesson.getTeacher())
                    && lesson.getTime() == this.lesson.getTime()) {
                return lesson;
            }
        }
        return null;
    }

    private Serializable getFilterDates() {
        Set<Long> dates = new HashSet<>();
        for (Lesson lesson : allLessons) {
            if (lesson.getSubject().equals(this.lesson.getSubject())
                    && lesson.getLevel() == this.lesson.getLevel()
                    && lesson.getSchool().equals(this.lesson.getSchool())
                    && lesson.getTeacher().equals(this.lesson.getTeacher())) {
                dates.add(lesson.getTime());
            }
        }
        return new ArrayList(dates);
    }

    private Serializable getFilterTeachers() {
        Set<Teacher> teachers = new HashSet<>();
        for (Lesson lesson : allLessons) {
            if (lesson.getSubject().equals(this.lesson.getSubject())
                    && lesson.getLevel() == this.lesson.getLevel()
                    && lesson.getSchool().equals(this.lesson.getSchool())) {
                teachers.add(lesson.getTeacher());
            }
        }
        return new ArrayList(teachers);
    }

    private Serializable getFilterSchools() {
        Set<School> schools = new HashSet<>();
        for (Lesson lesson : allLessons) {
            if (lesson.getSubject().equals(this.lesson.getSubject())
                    && lesson.getLevel() == this.lesson.getLevel()) {
                schools.add(lesson.getSchool());
            }
        }
        return new ArrayList(schools);
    }

    private ArrayList<Integer> getFilterLevelsBySubject() {
        Set<Integer> levels = new HashSet<>();
        for (Lesson lesson : allLessons) {
            if (lesson.getSubject().equals(this.lesson.getSubject())) {
                levels.add(lesson.getLevel());
            }
        }
        return new ArrayList(levels);
    }

    private ArrayList<Integer> getRelevantFilter() {
        Set<String> subjects = new HashSet<>();
        for (Lesson lesson : allLessons) {
            subjects.add(lesson.getSubject());
        }

        return new ArrayList(subjects);
    }

    public void handlePrevButton() {
        prevBt.setEnabled(!(getCurrentFragment() instanceof SelectSubjectFragment));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
