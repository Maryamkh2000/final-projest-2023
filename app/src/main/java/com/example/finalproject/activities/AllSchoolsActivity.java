package com.example.finalproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.finalproject.Logic.School;
import com.example.finalproject.R;
import com.example.finalproject.adapters.SchoolsAdapter;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class AllSchoolsActivity extends ListActivity {

    private ArrayList<School> schools = new ArrayList<>();
    private SchoolsAdapter schoolsAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        schoolsAdapter = new SchoolsAdapter();
        schoolsAdapter.setItems(schools);

        Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setVisibility(View.VISIBLE);
        list.setAdapter(schoolsAdapter);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AllSchoolsActivity.this, CreateSchoolActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchSchools();
    }

    private void fetchSchools(){
        swipeRefreshLayout.setRefreshing(true);
        FirebaseDatabaseManager.getAllSchools(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                swipeRefreshLayout.setRefreshing(false);
                if (task.isSuccessful()) {
                    schools.clear();
                    for (DocumentSnapshot dataSnapshot : task.getResult().getDocuments()) {
                        schools.add(dataSnapshot.toObject(School.class));
                    }
                    schoolsAdapter.notifyDataSetChanged();
                }
            }
        });
    }


    @Override
    protected String getToolbarTitle() {
        return getString(R.string.all_schools);
    }

    @Override
    protected void reload() {
        super.reload();
        fetchSchools();
    }
}
