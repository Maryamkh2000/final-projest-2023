package com.example.finalproject.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.LessonReview;
import com.example.finalproject.Logic.Student;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class CreateReviewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_activity_layout);
        Lesson lesson = (Lesson) getIntent().getSerializableExtra("lesson");
        ImageView back = findViewById(R.id.back);
        EditText notesET = findViewById(R.id.notesEditText);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        RatingBar ratingBar = findViewById(R.id.ratingBar);
        Button addBtn = findViewById(R.id.addBtn);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (notesET.getText().toString().equals("")) {
                    AlertDialogUtils.showAlertDialog(CreateReviewActivity.this,
                            getString(R.string.error), getString(R.string.please_add_note));
                } else {
                    LessonReview lessonReview = new LessonReview(SessionUser.currentUser.getId(), ratingBar.getRating(),
                            notesET.getText().toString(), ((Student) SessionUser.currentUser).copy());
                    lesson.getLessonReviews().put(lessonReview.getId(), lessonReview);
                    FirebaseDatabaseManager.addReview(lessonReview, lesson, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), R.string.review_is_added_successfully, Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                AlertDialogUtils.showAlertDialog(getApplicationContext(), getString(R.string.error), task.getException().getMessage());
                            }
                        }
                    });

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}