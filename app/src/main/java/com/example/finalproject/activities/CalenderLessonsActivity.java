package com.example.finalproject.activities;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;
import com.example.finalproject.adapters.LessonInCalenderAdapter;
import com.example.finalproject.eventdecorator.EventDecorator;
import com.example.finalproject.session.SessionUser;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CalenderLessonsActivity extends AppCompatActivity {

    private MaterialCalendarView calendarView;
    private RecyclerView recyclerView;
    private List<Lesson> lessons = new ArrayList<>();
    private Calendar calendar = Calendar.getInstance();
    private LessonInCalenderAdapter adapter;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calender_with_list_layout);
        ((Toolbar)findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        calendarView = findViewById(R.id.calender);
        recyclerView = findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new LessonInCalenderAdapter(this, lessons);
        recyclerView.setAdapter(adapter);
        lessons.addAll(SessionUser.currentUser.getLessons().values());
        List<Lesson> filteredLesson = lessons.stream().filter(new Predicate<Lesson>() {
            @Override
            public boolean test(Lesson lesson) {
                return lesson.getTime() == calendar.getTimeInMillis();
            }
        }).collect(Collectors.toList());
        adapter.setItems(filteredLesson);

        HashSet<CalendarDay> eventDates = new HashSet<>();

        for (Lesson lesson : lessons) {
            calendar.setTimeInMillis(lesson.getTime());
            eventDates.add(CalendarDay.from(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)));
        }

        EventDecorator eventDecorator = new EventDecorator(
                this,
                R.drawable.marker_drawable, // Replace with your marker drawable
                ContextCompat.getColor(this, R.color.red), // Replace with your marker color
                eventDates
        );

        calendarView.addDecorator(eventDecorator);

        calendarView.setSelectedDate(CalendarDay.today());
        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                CalenderLessonsActivity.this.onDateSelected(date);
            }
        });

        onDateSelected(calendarView.getSelectedDate());
    }

    private void onDateSelected(@NonNull CalendarDay date) {
        List<Lesson> filteredLessons = lessons.stream().filter(new Predicate<Lesson>() {
            @Override
            public boolean test(Lesson lesson) {
                calendar.setTimeInMillis(lesson.getTime());
                return calendar.get(Calendar.YEAR) == date.getYear()
                        && calendar.get(Calendar.MONTH) + 1 == date.getMonth() &&
                        calendar.get(Calendar.DAY_OF_MONTH) == date.getDay();
            }
        }).collect(Collectors.toList());
        adapter.setItems(filteredLessons);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

}
