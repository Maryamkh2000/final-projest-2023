package com.example.finalproject.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.LessonReview;
import com.example.finalproject.R;
import com.example.finalproject.adapters.ReviewsAdapter;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

public class ReviewsActivity extends ListActivity {

    private List<LessonReview> items = new ArrayList<>();
    private Lesson lesson;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lesson = (Lesson) getIntent().getExtras().getSerializable("lesson");
        Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setVisibility(SessionUser.isStudent() ? View.VISIBLE : View.GONE);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReviewsActivity.this, CreateReviewActivity.class);
                intent.putExtra("lesson", lesson);
                startActivity(intent);
            }
        });
        list.setAdapter(new ReviewsAdapter(this, items, new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                AlertDialogUtils.showAlertConfirmationDialog(ReviewsActivity.this, getString(R.string.alert),
                        getString(R.string.are_you_sure_you_want_to_delete_this_review), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                FirebaseDatabaseManager.removeReview(lesson, new OnSuccessListener() {
                                    @Override
                                    public void onSuccess(Object o) {
                                        Toast.makeText(ReviewsActivity.this, R.string.review_deleted_successfully, Toast.LENGTH_LONG).show();
                                        items.remove(position);
                                        list.getAdapter().notifyItemRemoved(position);
                                    }
                                });
                            }
                        });

            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        items.clear();
        fetchData();
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.lessons_reviews);
    }

    @Override
    protected void reload() {
        super.reload();
        fetchData();
    }

    private void fetchData() {
        swipeRefreshLayout.setRefreshing(true);
        FirebaseDatabaseManager.getLessonById(lesson.getId(), new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                swipeRefreshLayout.setRefreshing(false);
                if (task.isSuccessful()) {
                    items.clear();
                    items.addAll(task.getResult().toObject(Lesson.class).getLessonReviews().values());
                    if (items.isEmpty()) {
                        AlertDialogUtils.showAlertDialog(
                                ReviewsActivity.this, getString(R.string.alert), getString(R.string.no_reviews));
                    } else {
                        list.getAdapter().notifyDataSetChanged();
                    }
                }
            }
        });
    }
}
