package com.example.finalproject.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.finalproject.Logic.CheckedFilterItem;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.School;
import com.example.finalproject.Logic.Student;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.bottomsheet.FilterBottomSheet;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.OnFilterClicked;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class StatisticActivity extends AppCompatActivity {


    private TextView revenues, studyHours,
            teacherEmail, teacherName, studentName, studentEmail, schoolAddress, schoolName,
            subject, school, users, level, date;
    private ImageView teacherImage, studentImage;
    private LinearLayout schoolContainer, teacherContainer, studentContainer;
    private CheckedFilterItem levelSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem schoolSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem dateSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem usersSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem subjectSelectedFilter = new CheckedFilterItem();
    private Set<CheckedFilterItem> subjectsFilter = new HashSet<>();
    private Set<CheckedFilterItem> schoolsFilter = new HashSet<>();
    private Set<CheckedFilterItem> datesFilter = new HashSet<>();
    private Set<CheckedFilterItem> usersFilter = new HashSet<>();
    private Set<CheckedFilterItem> levelsFilter = new HashSet<>();
    private CheckedFilterItem allFilter;
    private List<Lesson> filteredLessons = new ArrayList<Lesson>();
    private List<Lesson> lessons = new ArrayList<Lesson>();
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private Map<Student, Integer> topStudent = new HashMap<>();
    private Map<Teacher, Integer> topTeacher =new HashMap<>();
    private Map<School, Integer> topSchool =new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistic_layout);
        ((Toolbar)findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        revenues = findViewById(R.id.revenues);
        studyHours = findViewById(R.id.studyHours);
        teacherEmail = findViewById(R.id.teacherEmail);
        teacherName = findViewById(R.id.teacherName);
        studentName = findViewById(R.id.studentName);
        subject = findViewById(R.id.subject);
        school = findViewById(R.id.school);
        users = findViewById(R.id.users);
        level = findViewById(R.id.level);
        date = findViewById(R.id.date);
        studentEmail = findViewById(R.id.studentEmail);
        schoolAddress = findViewById(R.id.schoolAddress);
        schoolName = findViewById(R.id.schoolName);
        teacherImage = findViewById(R.id.teacherImage);
        studentImage = findViewById(R.id.studentImage);
        schoolContainer = findViewById(R.id.schoolContainer);
        teacherContainer = findViewById(R.id.teacherContainer);
        studentContainer = findViewById(R.id.studentContainer);
        initFilters();
        fetchAllLesson();
    }

    private void initFilters() {
        allFilter = new CheckedFilterItem(null, getString(R.string.all), "", true);
        subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(subjectsFilter);
                allFilter.setChecked(subjectSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.subject), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        subjectSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(datesFilter);
                allFilter.setChecked(dateSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.dateo), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        dateSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(levelsFilter);
                allFilter.setChecked(levelSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.level2), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        levelSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        users.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(usersFilter);
                allFilter.setChecked(usersSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.teacher1), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        usersSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(schoolsFilter);
                allFilter.setChecked(schoolSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.school5), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        schoolSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });
    }

    private void showBottomSheet(List<CheckedFilterItem> items, String title, OnFilterClicked onFilterClicked) {
        FilterBottomSheet filterBottomSheet = new FilterBottomSheet(items, title, onFilterClicked);
        filterBottomSheet.show(getSupportFragmentManager(), "");
    }

    private void arrangeListByFilters() {
        filteredLessons.clear();
        topStudent.clear();
        topSchool.clear();
        topTeacher.clear();
        for (Lesson lesson : lessons) {
            if (shouldAddToFilterList(lesson)) {
                filteredLessons.add(lesson);
            }
        }
        populateScreenData();
    }

    private void populateScreenData() {
        double revenues = 0;
        int studyHours = 0;
        for (Lesson lesson: filteredLessons) {
            revenues += lesson.getPrice();
            studyHours += lesson.getDuration();
            topTeacher.put(lesson.getTeacher(), topTeacher.getOrDefault(lesson.getTeacher(), 0) +1);
            topSchool.put(lesson.getSchool(), topSchool.getOrDefault(lesson.getSchool(), 0) +1);
            for (Student student : lesson.getStudents().values()) {
                topStudent.put(student, topStudent.getOrDefault(student, 0) +1);
            }

        }

        if (studyHours < 60) {
            this.studyHours.setText(getString(R.string.number_mins, studyHours));
        } else {
            this.studyHours.setText(getString(R.string.number_hours, studyHours/60));
        }

        this.revenues.setText(String.valueOf(revenues));

        Teacher topT = null;
        Student topS = null;
        School topSc = null;

        int maxValue = Integer.MIN_VALUE;

        for (Map.Entry<Teacher, Integer> entry : topTeacher.entrySet()) {
            Teacher teacher = entry.getKey();
            int value = entry.getValue();

            if (value > maxValue) {
                maxValue = value;
                topT = teacher;
            }
        }

         maxValue = Integer.MIN_VALUE;

        for (Map.Entry<Student, Integer> entry : topStudent.entrySet()) {
            Student student = entry.getKey();
            int value = entry.getValue();

            if (value > maxValue) {
                maxValue = value;
                topS = student;
            }
        }

        maxValue = Integer.MIN_VALUE;
        for (Map.Entry<School, Integer> entry : topSchool.entrySet()) {
            School school = entry.getKey();
            int value = entry.getValue();

            if (value > maxValue) {
                maxValue = value;
                topSc = school;
            }
        }

        if (topT != null) {
            Glide.with(this)
                    .load(topT.getImageUrl()).circleCrop()
                    .placeholder(R.drawable.baseline_person_24).into(teacherImage);
            teacherContainer.setVisibility(View.VISIBLE);
            teacherName.setText(topT.getName());
            teacherEmail.setText(topT.getEmail());
        }
        else {
            teacherContainer.setVisibility(View.GONE);
        }


        if (topS != null) {
            Glide.with(this)
                    .load(topS.getImageUrl()).circleCrop()
                    .placeholder(R.drawable.baseline_person_24).into(studentImage);
            studentContainer.setVisibility(View.VISIBLE);
            studentName.setText(topS.getName());
            studentEmail.setText(topS.getEmail());
        } else {
            studentContainer.setVisibility(View.GONE);
        }

        if (topSc != null) {
            schoolName.setText(topSc.getName());
            schoolAddress.setText(topSc.getLocation());
            schoolContainer.setVisibility(View.VISIBLE);
        } else {
            schoolContainer.setVisibility(View.GONE);
        }


    }

    private boolean shouldAddToFilterList(Lesson lesson) {
        String date =  format.format(new Date(lesson.getTime()));

        return (lesson.getSubject().equals(subjectSelectedFilter.getId()) || subjectSelectedFilter.getId() == null)
                && ( usersSelectedFilter.getId() == null || lesson.getTeacher().getId().equals(usersSelectedFilter.getId()) || lesson.getStudents().keySet().contains(usersSelectedFilter.getId()))
                && (String.valueOf(lesson.getLevel()).equals(levelSelectedFilter.getId()) || levelSelectedFilter.getId() == null)
                && (lesson.getSchool().getId().equals(schoolSelectedFilter.getId()) || schoolSelectedFilter.getId() == null)
                && (date.equals(dateSelectedFilter.getId()) || dateSelectedFilter.getId() == null);
    }

    private void initFilterItems(Lesson lesson) {
        addToFilters(lesson);
        if (shouldAddToFilterList(lesson)) {
            filteredLessons.add(lesson);
        }
    }

    private void addToFilters(Lesson lesson) {
        subjectsFilter.add(new CheckedFilterItem(lesson.getSubject(), lesson.getSubject(), null, false));
        levelsFilter.add(new CheckedFilterItem(String.valueOf(lesson.getLevel()), String.valueOf(lesson.getLevel()), null, false));
        usersFilter.add(new CheckedFilterItem(lesson.getTeacher().getId(), lesson.getTeacher().getName(), null, false));
        for (Student student : lesson.getStudents().values()) {
            usersFilter.add(new CheckedFilterItem(String.valueOf(student.getId()),student.getName(), null, false));
        }
        schoolsFilter.add(new CheckedFilterItem(String.valueOf(lesson.getSchool().getId()), lesson.getSchool().getName(),  lesson.getSchool().getLocation(), false));
        String date =  format.format(new Date(lesson.getTime()));
        datesFilter.add(new CheckedFilterItem(date, date,  null, false));
    }

    private void fetchAllLesson() {
        FirebaseDatabaseManager.getAllLessons(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    filteredLessons.clear();
                    List<DocumentSnapshot> documentSnapshots = task.getResult().getDocuments();
                    for (DocumentSnapshot snapshot: documentSnapshots) {
                        Lesson lesson = snapshot.toObject(Lesson.class);
                        initFilterItems(lesson);
                        lessons.add(lesson);
                    }
                    arrangeListByFilters();
                } else {
                    AlertDialogUtils.showAlertDialog(getApplicationContext(), getString(R.string.error), task.getException().getMessage());
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}