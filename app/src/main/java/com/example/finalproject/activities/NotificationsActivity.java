package com.example.finalproject.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.finalproject.Logic.Notification;
import com.example.finalproject.R;
import com.example.finalproject.adapters.NotificationsAdapter;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class NotificationsActivity extends ListActivity {

    private NotificationsAdapter adapter;
    private List<Notification> notifications = new ArrayList();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new NotificationsAdapter(this, notifications, new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                AlertDialogUtils.showAlertConfirmationDialog(NotificationsActivity.this, getString(R.string.alert),
                        getString(R.string.are_you_sure_you_want_to_delete_this_notification), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Notification notification = notifications.remove(position);
                                FirebaseDatabaseManager.removeNotification(notification, new OnSuccessListener() {
                                    @Override
                                    public void onSuccess(Object o) {
                                        Toast.makeText(NotificationsActivity.this,  getString(R.string.notification_deleted), Toast.LENGTH_LONG).show();
                                    }
                                });
                                adapter.notifyItemRemoved(position);
                            }
                        });
            }
        });
        list.setAdapter(adapter);
        fetchData();
    }

    private void fetchData() {
        swipeRefreshLayout.setRefreshing(true);
        FirebaseDatabaseManager.getAllNotifications(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                swipeRefreshLayout.setRefreshing(false);
                if (task.isSuccessful()) {
                    notifications.clear();
                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                        notifications.add(documentSnapshot.toObject(Notification.class));
                    }
                    if (notifications.isEmpty()) {
                        AlertDialogUtils.showAlertDialog(NotificationsActivity.this,
                                getString(R.string.no_result), getString(R.string.no_notifications));
                    } else {
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(NotificationsActivity.this,  task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.notifications);
    }

    @Override
    protected void reload() {
        super.reload();
        fetchData();
    }
}