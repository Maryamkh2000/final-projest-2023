package com.example.finalproject.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.example.finalproject.Logic.Student;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.Logic.User;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class UserProfileActivity extends AppCompatActivity {

    public static final String USER  = "USER";
    private EditText emailEt;
    private EditText phoneEt;
    private ImageView profilePicture;
    private EditText ageEt;
    private Button updateBtn;
    private Spinner addressSpinner;
    private EditText nameET;
    Button camera;
    Button gallery;
    String imageDownloadUrl;
    private Bitmap imageBitmap;
    private StorageReference storageRef;
    private User user;
    private ArrayAdapter<String> addressAdapter;


    private ActivityResultLauncher<Intent> cameraActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Bitmap photo = (Bitmap) result.getData().getExtras().get("data");
                    imageBitmap = photo;
                    profilePicture.setImageBitmap(photo);
                }
            }
    );

    private ActivityResultLauncher<Intent> galleryActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    try {
                        Uri uri = result.getData().getData();
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        imageBitmap = bitmap;
                        profilePicture.setImageBitmap(bitmap);
                    } catch (Exception e) {
                    }
                }
            }
    );
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_layout);
        ((Toolbar)findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        addressAdapter = new ArrayAdapter<>(this, R.layout.spinner_list_item, getResources().getStringArray(R.array.Regions));
        storageRef = FirebaseStorage.getInstance().getReference();
        camera = (Button) findViewById(R.id.camera);
        gallery = (Button) findViewById(R.id.gallery);
        emailEt = (EditText) findViewById(R.id.emailEt);
        phoneEt = (EditText) findViewById(R.id.phoneEt);
        profilePicture = (ImageView) findViewById(R.id.profilePicture);
        ageEt = (EditText) findViewById(R.id.ageEt);
        updateBtn = (Button) findViewById(R.id.updateBtn);
        addressSpinner = findViewById(R.id.addressSpinner);
        addressSpinner.setAdapter(addressAdapter);
        nameET = findViewById(R.id.nameET);
        user = (User) getIntent().getSerializableExtra(USER);
        if (user == null) {
            fetchSessionUser();
        } else {
            nameET.setEnabled(false);
            phoneEt.setEnabled(false);
            emailEt.setEnabled(false);
            ageEt.setEnabled(false);
            addressSpinner.setEnabled(false);
            updateBtn.setVisibility(View.GONE);
            camera.setVisibility(View.GONE);
            gallery.setVisibility(View.GONE);
            fetchTeacherUser();
        }

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogUtils.showAlertConfirmationDialog(UserProfileActivity.this, getString(R.string.alert),
                        getString(R.string.save_changes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                user.setName(nameET.getText().toString());
                                user.setAge(ageEt.getText().toString());
                                user.setPhone(phoneEt.getText().toString());
                                user.setRegion(addressSpinner.getSelectedItem().toString());
                                if (imageBitmap != null) {
                                    uploadImageToStorage(new Runnable() {
                                        @Override
                                        public void run() {
                                            user.setImageUrl(imageDownloadUrl);
                                            updateUser();
                                        }
                                    });
                                } else {
                                    updateUser();
                                }
                            }
                        });
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPhotoGallery();
            }
        });
    }

    private void fetchTeacherUser() {
        FirebaseDatabaseManager.getTeacher(user.getId(), new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    user = task.getResult().toObject(Teacher.class);
                    populateScreenData();
                }

            }
        }, false);
    }

    private void populateScreenData() {
        emailEt.setText(user.getEmail());
        phoneEt.setText(user.getPhone());
        ageEt.setText(user.getAge());
        nameET.setText(user.getName());

        Glide.with(UserProfileActivity.this)
                .load(user.getImageUrl()).circleCrop()
                .placeholder(R.drawable.baseline_person_24).into(profilePicture);
        addressSpinner.setSelection(addressAdapter.getPosition(user.getRegion()));
    }

    private void fetchSessionUser() {
        FirebaseDatabaseManager.getCurrentUser(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    if (SessionUser.isStudent()) {
                        user = task.getResult().toObject(Student.class);
                    } else {
                        user = task.getResult().toObject(Teacher.class);
                    }
                    populateScreenData();
                }
            }
        });
    }

    private void updateUser() {
        FirebaseDatabaseManager.updateUser(user, new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), R.string.update_successfully, Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    AlertDialogUtils.showAlertDialog(getApplicationContext(), getString(R.string.error), task.getException().getMessage());
                }
            }
        });

    }
    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraActivityResultLauncher.launch(takePictureIntent);
    }

    private void openPhotoGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        galleryActivityResultLauncher.launch(intent);
    }

    private void uploadImageToStorage(Runnable onDone) {
        // the image name should be the user uuid - uniqe
        StorageReference imageRef = storageRef.child("images/" + SessionUser.getUserId() + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageData = baos.toByteArray();
        // Upload file to Firebase Storage
        UploadTask uploadTask = imageRef.putBytes(imageData);
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            imageRef.getDownloadUrl().addOnSuccessListener(uri -> {
                imageDownloadUrl = uri.toString();
                onDone.run();
            });
        }).addOnFailureListener(e -> {
            // Handle failed upload
            Log.e("TAG", "Upload failed: " + e.getMessage());
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}