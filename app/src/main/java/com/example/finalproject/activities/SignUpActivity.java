package com.example.finalproject.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.Logic.User;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "Sign up activity";


    private ImageView vector;
    private ImageView vector_ek1;
    private ImageView arrow_left_by_streamlinehq_ek1;
    private ImageView __img___codicon_account;
    private Button sign_up1;
    private TextView insertYourUserNameMessage;
    private TextView insertYourPhoneMessage;
    private TextView EmailMessage;
    private TextView passwordMessage;
    private TextView rePasswordMessage;
    private TextView nameMessage;
    private TextView typeMessage;
    private TextView ageMessage;
    private EditText editTextPhone;
    private EditText userNameEditText1;
    private EditText emailEditText;
    private EditText nameEditText;
    private Spinner addressSpinner5;
    private EditText ageEditText;
    private EditText passwordEditText1;
    private EditText rePasswordEditText;
    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;
    private FirebaseAuth auth;
    private Spinner typeSpinner;
    private Spinner genderSpinner;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String id;
    private Bitmap imageBitmap;
    private StorageReference storageRef;
    Button camera;
    Button gallery;
    String imageDownloadUrl;

    private ActivityResultLauncher<Intent> cameraActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Bitmap photo = (Bitmap) result.getData().getExtras().get("data");
                    imageBitmap = photo;
                    __img___codicon_account.setImageBitmap(photo);
                }
            }
    );

    private ActivityResultLauncher<Intent> galleryActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    try {
                        Uri uri = result.getData().getData();
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        imageBitmap = bitmap;
                        __img___codicon_account.setImageBitmap(bitmap);
                    } catch (Exception e) {
                    }
                }
            }
    );

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_layout);
        auth = FirebaseAuth.getInstance();
        FirebaseStorage storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        arrow_left_by_streamlinehq_ek1 = (ImageView) findViewById(R.id.arrow_left_by_streamlinehq_ek1);
        __img___codicon_account = (ImageView) findViewById(R.id.__img___codicon_account);
        sign_up1 = (Button) findViewById(R.id.sign_up1);
        vector_ek1 = (ImageView) findViewById(R.id.vector_ek1);
        vector = (ImageView) findViewById(R.id.vector);
        insertYourPhoneMessage = (TextView) findViewById(R.id.insertYourPhoneMessage);
        EmailMessage = (TextView) findViewById(R.id.EmailMessage);
        passwordMessage = (TextView) findViewById(R.id.passwordMessage);
        rePasswordMessage = (TextView) findViewById(R.id.rePasswordMessage);
        typeMessage = (TextView) findViewById(R.id.typeMessage);
        ageMessage = (TextView) findViewById(R.id.ageMessage);
        editTextPhone = (EditText) findViewById(R.id.editTextPhone);
        emailEditText = (EditText) findViewById(R.id.emailEditText);
        addressSpinner5 = (Spinner) findViewById(R.id.addressSpinner5);
        ageEditText = (EditText) findViewById(R.id.ageEditText);
        passwordEditText1 = (EditText) findViewById(R.id.passwordEditText1);
        rePasswordEditText = (EditText) findViewById(R.id.rePasswordEditText);
        typeSpinner = (Spinner) findViewById(R.id.typeSpinner);
        genderSpinner = (Spinner) findViewById(R.id.genderSpinner);
        camera = (Button) findViewById(R.id.camera);
        gallery = (Button) findViewById(R.id.gallery);
        nameMessage = findViewById(R.id.nameMessage);
        nameEditText = findViewById(R.id.nameEditText);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCamera();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPhotoGallery();
            }
        });

        sign_up1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int check = 1;
                if (nameEditText.getText().toString().equals("")) {
                    nameMessage.setVisibility(View.VISIBLE);
                    check = 0;
                }
                if (editTextPhone.getText().toString().equals("")) {
                    insertYourPhoneMessage.setVisibility(View.VISIBLE);
                    check = 0;
                }
                if (emailEditText.getText().toString().equals("")) {
                    EmailMessage.setVisibility(View.VISIBLE);
                    check = 0;
                }
                if (ageEditText.getText().toString().equals("")) {
                    ageMessage.setVisibility(View.VISIBLE);
                    check = 0;
                }
                if (passwordEditText1.toString().equals("")) {
                    passwordMessage.setVisibility(View.VISIBLE);
                    check = 0;
                }
                if (rePasswordEditText.getText().toString().equals("")) {
                    rePasswordMessage.setVisibility(View.VISIBLE);
                    check = 0;
                }
                if (!passwordEditText1.getText().toString().equals(rePasswordEditText.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "you have to re enter your password correctly!", Toast.LENGTH_LONG).show();
                    check = 0;
                } else if (check == 1) {

                    auth.createUserWithEmailAndPassword(emailEditText.getText().toString(), passwordEditText1.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser firebaseUser = task.getResult().getUser();
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(typeSpinner.getSelectedItem().toString()).build();
                                firebaseUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        createUserProfile();
                                    }
                                });
                            } else {
                                AlertDialogUtils.showAlertDialog(SignUpActivity.this, getString(R.string.error), task.getException().getMessage());
                            }
                        }
                    });
                }
            }
        });
        vector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rePasswordEditText.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                    rePasswordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    rePasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });
        vector_ek1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passwordEditText1.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
                    passwordEditText1.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    passwordEditText1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        };
        this.getOnBackPressedDispatcher().addCallback(this, callback);

        arrow_left_by_streamlinehq_ek1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        __img___codicon_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

    }

    private void createUserProfile() {
        if (imageBitmap != null) {
            uploadImageToStorage(new Runnable() {
                @Override
                public void run() {
                    saveProfile();
                }
            });
        } else {
            saveProfile();
        }
    }

    private void saveProfile() {
        User user = new User(FirebaseAuth.getInstance().getCurrentUser().getUid(),
                nameEditText.getText().toString(),
                FirebaseAuth.getInstance().getCurrentUser().getEmail(),
                editTextPhone.getText().toString(),
                addressSpinner5.getSelectedItem().toString(),
                imageDownloadUrl,
                genderSpinner.getSelectedItem().toString(),
                ageEditText.getText().toString());
        FirebaseDatabaseManager.saveUser(user, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Intent intent;
                    SessionUser.currentUser = user;
                    if (SessionUser.isTeacher()) {
                        intent = new Intent(SignUpActivity.this, TeacherHomePageActivity.class);
                    } else {
                        intent = new Intent(SignUpActivity.this, StudentHomePageActivity.class);
                    }
                    startActivity(intent);
                    finish();
                } else {
                    AlertDialogUtils.showAlertDialog(SignUpActivity.this, getString(R.string.error), task.getException().getMessage());
                }
            }
        });
    }

    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraActivityResultLauncher.launch(takePictureIntent);
    }

    private void openPhotoGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        galleryActivityResultLauncher.launch(intent);
    }

    private void uploadImageToStorage(Runnable onDone) {
        // the image name should be the user uuid - uniqe
        StorageReference imageRef = storageRef.child("images/" + SessionUser.getUserId() + ".jpg");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageData = baos.toByteArray();
        // Upload file to Firebase Storage
        UploadTask uploadTask = imageRef.putBytes(imageData);
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            imageRef.getDownloadUrl().addOnSuccessListener(uri -> {
                imageDownloadUrl = uri.toString();
                onDone.run();
            });
        }).addOnFailureListener(e -> {
            // Handle failed upload
            Log.e("TAG", "Upload failed: " + e.getMessage());
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

	
	