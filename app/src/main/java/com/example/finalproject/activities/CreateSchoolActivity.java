package com.example.finalproject.activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.Logic.GeoPoint;
import com.example.finalproject.Logic.School;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class CreateSchoolActivity extends AppCompatActivity {
    static final int REQUEST_LOCATION = 3;

    private TextView address;
    private Address location;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_school_layout);
        EditText name = findViewById(R.id.nameET);
        address = findViewById(R.id.addressET);
        Button addBtn = findViewById(R.id.addBtn);
        Spinner opening = findViewById(R.id.opening);
        Spinner closing = findViewById(R.id.closing);
        ImageView backBT = findViewById(R.id.back);

        backBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().equals("")) {
                    AlertDialogUtils.showAlertDialog(CreateSchoolActivity.this,
                            getString(R.string.error), getString(R.string.please_add_school_name));
                } else if (address.getText().toString().equals("")) {
                    AlertDialogUtils.showAlertDialog(CreateSchoolActivity.this,
                            getString(R.string.error), getString(R.string.please_add_school_address));
                } else {
                    FirebaseDatabaseManager.saveSchool(new School(String.valueOf(System.currentTimeMillis()), name.getText().toString(),
                            address.getText().toString(),  new GeoPoint(location.getLatitude(), location.getLongitude()), Integer.parseInt((String) opening.getSelectedItem()),
                            Integer.parseInt((String) closing.getSelectedItem())), new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), R.string.shool_added, Toast.LENGTH_LONG).show();
                                finish();
                            } else {
                                AlertDialogUtils.showAlertDialog(CreateSchoolActivity.this,
                                        getString(R.string.error), task.getException().getMessage());
                            }
                        }
                    });
                }

            }
        });
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateSchoolActivity.this, FindMyLocationActivity.class);
                startActivityForResult(intent, REQUEST_LOCATION);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_LOCATION) {
            location = (Address) data.getParcelableExtra("address");
            address.setText(location.getAddressLine(0));
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
