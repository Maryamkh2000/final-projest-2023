package com.example.finalproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.R;
import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;
import com.example.finalproject.session.SessionUser;
import com.google.firebase.auth.FirebaseAuth;

public class StudentHomePageActivity extends AppCompatActivity {

	
	private TextView student;
	private ImageView signout;
	private Button lessonsBT, futureLesson, myProfile;
	private ImageView notification;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page_for_student);

		signout = (ImageView) findViewById(R.id.imageView2);
		lessonsBT = (Button) findViewById(R.id.myClassesBtn);
		notification = (ImageView) findViewById(R.id.notification);
		myProfile = (Button) findViewById(R.id.myProfile);
		futureLesson = findViewById(R.id.futureLesson);

		FirebaseMessagingManager.subscribeToPush("Daniel");
		OnBackPressedCallback callback = new OnBackPressedCallback(true) {
			@Override
			public void handleOnBackPressed() {
				Bundle extras = new Bundle();

				Intent intent = new Intent(StudentHomePageActivity.this, LoginActivity.class);
				intent.putExtras(extras);
				startActivity(intent);           }
		};
		this.getOnBackPressedDispatcher().addCallback(this, callback);

		signout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FirebaseAuth.getInstance().signOut();
				finish();
			}
		});

		lessonsBT.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(StudentHomePageActivity.this, UserLessonsActivity.class);
				intent.putExtra(UserLessonsActivity.USER_ID, SessionUser.getUserId());
				intent.putExtra(UserLessonsActivity.IS_TEACHER, false);
				startActivity(intent);

			}
		});

		myProfile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle extras = new Bundle();
				Intent intent = new Intent(StudentHomePageActivity.this, UserProfileActivity.class);
				intent.putExtras(extras);
				startActivity(intent);
			}
		});

		notification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle extras = new Bundle();
				Intent intent = new Intent(StudentHomePageActivity.this , NotificationsActivity.class);
				startActivity(intent);
			}
		});

		futureLesson.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(StudentHomePageActivity.this, RegisterToFutureLessonActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
	
	