package com.example.finalproject.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.finalproject.Logic.Enums.Step;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.LessonRequest;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.fragments.SelectLevelsFragment;
import com.example.finalproject.fragments.SelectSchoolFragment;
import com.example.finalproject.fragments.SelectSubjectFragment;
import com.example.finalproject.interfaces.HasBack;
import com.example.finalproject.interfaces.HasNext;
import com.example.finalproject.interfaces.OnNextClicked;
import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class RegisterToFutureLessonActivity extends AppCompatActivity implements HasNext, HasBack {

    public static final String LESSON = "LESSON";
    public static final String IS_UPDATE = "IS_UPDATE";

    private LessonRequest lessonRequest = new LessonRequest();
    private Lesson lesson = new Lesson();
    private Step currentStep = Step.Subjects;
    private Button prevBt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        ImageView close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Button nextBt = findViewById(R.id.next);
        nextBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((OnNextClicked)getcurrentFragment()).onNextClicked(lesson);
            }
        });
        prevBt = findViewById(R.id.prev);
        prevBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().popBackStack();
            }
        });

        startFlow();
    }

    private void startFlow() {
        SelectSubjectFragment fragment =  new SelectSubjectFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    private Fragment getcurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.container);
    }

    @Override
    public void goNext() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        Fragment destination = null;
        if (fragment instanceof SelectSubjectFragment){
            destination = new SelectLevelsFragment();
        } else if (fragment instanceof  SelectLevelsFragment) {
            destination = new SelectSchoolFragment();
        } else {
            registerToFutureLesson();
            return;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, destination)
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    private void registerToFutureLesson() {
        lessonRequest.setSubject(lesson.getSubject());
        lessonRequest.setSchool(lesson.getSchool());
        lessonRequest.setLevel(lesson.getLevel());
        lessonRequest.setId(String.valueOf(System.currentTimeMillis()));
        FirebaseDatabaseManager.registerToFutureLesson(lessonRequest, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
               if (task.isSuccessful()) {
                   Toast.makeText(RegisterToFutureLessonActivity.this, R.string.the_request_is_created_successfully, Toast.LENGTH_LONG).show();
                   FirebaseMessagingManager.subscribeToPush(lessonRequest.getId());
                   finish();
               } else {
                   AlertDialogUtils.showAlertDialog(RegisterToFutureLessonActivity.this, getString(R.string.error), task.getException().getMessage());
               }
            }
        });
    }

    public void handlePrevButton() {
        prevBt.setEnabled(!(getcurrentFragment() instanceof SelectSubjectFragment));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
