package com.example.finalproject.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.Logic.Feedback;
import com.example.finalproject.R;
import com.example.finalproject.addattachmentmanager.AddAttachmentManager;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

public class CreateFeedbackForStudentActivity extends AppCompatActivity {

    public static final String LESSON_ID = "LESSON_ID";
    public static final String STUDENT_ID = "STUDENT_ID";
    public static final String TEACHER_ID = "TEACHER_ID";
    private String lessonId;
    private String teacherId;
    private String studentId;
    private EditText feedbackET;
    private Uri myUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_activity_layout);
        lessonId = getIntent().getStringExtra(LESSON_ID);
        studentId = getIntent().getStringExtra(STUDENT_ID);
        teacherId = getIntent().getStringExtra(TEACHER_ID);

        ImageView back = findViewById(R.id.back);
        feedbackET = findViewById(R.id.feedbackET);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFeedback();
                if (feedbackET.getText().toString().equals("")) {
                    AlertDialogUtils.showAlertDialog(CreateFeedbackForStudentActivity.this,
                            getString(R.string.error), getString(R.string.please_add_feedback));
                } else {

                }
            }
        });

        Button addAttachment = findViewById(R.id.addAttachment);
        addAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddAttachmentManager.openAttachmentIntent(CreateFeedbackForStudentActivity.this, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ProgressDialog progressDialog = new ProgressDialog(this);

        if (resultCode == RESULT_OK && data != null) {
            progressDialog.setTitle(R.string.uploading);
            progressDialog.show();
            Uri uri = data.getData();
            StorageReference ref = FirebaseStorage.getInstance().getReference().child(AddAttachmentManager.getFilePath());
            ref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            progressDialog.hide();
                            myUri = uri;
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.hide();
                        }
                    });
                    Toast.makeText(CreateFeedbackForStudentActivity.this, getString(R.string.file_uploaded), Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.hide();
                    Toast.makeText(CreateFeedbackForStudentActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                    double progress = (100.0 * snapshot.getBytesTransferred()) / snapshot.getTotalByteCount();
                    progressDialog.setMessage("Uploaded:" + (int) progress + "%");
                }
            });

        }
    }

    private void saveFeedback() {
        if (feedbackET.getText().toString().equals("")) {
            AlertDialogUtils.showAlertDialog(CreateFeedbackForStudentActivity.this,
                    getString(R.string.error), getString(R.string.please_add_feedback));
        } else {
            Feedback feedback = new Feedback(UUID.randomUUID().toString(),
                    feedbackET.getText().toString(), myUri != null ? myUri.toString() : null, teacherId, studentId, lessonId);
            FirebaseDatabaseManager.addFeedback(feedback, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        FirebaseMessagingManager.sendNotification(CreateFeedbackForStudentActivity.this, lessonId, getString(R.string.you_have_new_feedback_from) + " " + SessionUser.currentUser.getName());
                        Toast.makeText(CreateFeedbackForStudentActivity.this, R.string.feedback_successfully_saved, Toast.LENGTH_SHORT).show();
                        CreateFeedbackForStudentActivity.this.finish();
                    } else {
                        Toast.makeText(CreateFeedbackForStudentActivity.this, "Failed " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}