package com.example.finalproject.activities;

import static com.example.finalproject.activities.FeedbacksActivity.LESSON;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.finalproject.Logic.CheckedFilterItem;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.Student;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.adapters.LessonsAdapter;
import com.example.finalproject.addattachmentmanager.AddAttachmentManager;
import com.example.finalproject.bottomsheet.FilterBottomSheet;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.interfaces.OnFilterClicked;
import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserLessonsActivity extends ListActivity {

    public static final String USER_ID  = "userId";
    public static final String IS_TEACHER  = "isTeacher";
    public static final String IS_ADMIN  = "isAdmin";


    private ImageView calender;
    private Button addBtn;
    private LinearLayout bottomContainer;
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private LessonsAdapter lessonsAdapter;
    private List<Lesson> filteredLessons = new ArrayList<Lesson>();
    private List<Lesson> lessons = new ArrayList<Lesson>();
    private boolean isAdmin;
    private CheckedFilterItem levelSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem schoolSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem dateSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem teacherSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem subjectSelectedFilter = new CheckedFilterItem();
    private CheckedFilterItem studentSelectedFilter = new CheckedFilterItem();
    private Set<CheckedFilterItem> subjectsFilter = new HashSet<>();
    private Set<CheckedFilterItem> schoolsFilter = new HashSet<>();
    private Set<CheckedFilterItem> datesFilter = new HashSet<>();
    private Set<CheckedFilterItem> teachersFilter = new HashSet<>();
    private Set<CheckedFilterItem> levelsFilter = new HashSet<>();
    private Set<CheckedFilterItem> studentsFilter = new HashSet<>();
    private CheckedFilterItem allFilter;
    private TextView subject, school, teacher, level, date, student;
    private SwipeRefreshLayout swipeToRefresh;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_with_title_and_filters_layout);
        initFilters();
        isAdmin = getIntent().getExtras().getBoolean(IS_ADMIN);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        toolbar.setTitle(getToolbarTitle());
        list = findViewById(R.id.list);
        list.setLayoutManager(new LinearLayoutManager(this));
        swipeToRefresh = findViewById(R.id.swipeToRefresh);
        swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLessons(isAdmin);
            }
        });
        swipeToRefresh.setRefreshing(true);
        calender = findViewById(R.id.calender);
        addBtn = (Button) findViewById(R.id.addBtn);
        bottomContainer = findViewById(R.id.bottomContainer);
        bottomContainer.setVisibility(!SessionUser.isAdmin() ? View.VISIBLE : View.GONE);
        lessonsAdapter = new LessonsAdapter(getApplicationContext(),
                filteredLessons, getDeleteListener(), getEditListener(), getShowStudentsListener(),
                getShowReviewsListener(), getUploadFileListener(), getOpenFileListener(),
                getFeedbacksListener(), getZoomLinkListener(), getEmailListener()
        ,getTeacherListener());
        list.setAdapter(lessonsAdapter);

        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                finish();         }
        };
        this.getOnBackPressedDispatcher().addCallback(this, callback);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionUser.isTeacher()) {
                    Intent intent = new Intent(UserLessonsActivity.this , AddOrUpdateLessonActivity.class);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(UserLessonsActivity.this , RegisterToLessonActivity.class);
                    startActivity(intent);
                }

            }
        });

        calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserLessonsActivity.this , CalenderLessonsActivity.class);
                startActivity(intent);
            }
        });

    }

    private OnClickListener getTeacherListener() {
        return  new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Teacher teacher = filteredLessons.get(position).getTeacher();
                Intent intent = new Intent(UserLessonsActivity.this, UserProfileActivity.class);
                intent.putExtra(UserProfileActivity.USER,teacher );
                startActivity(intent);
            }
        };
    }

    private OnClickListener getEmailListener() {
        return  new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + filteredLessons.get(position).getTeacher().getEmail()));
                startActivity(Intent.createChooser(emailIntent, getString(R.string.send_email)));
            }
        };
    }

    private OnClickListener getFeedbacksListener() {
        return  new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Intent intent = new Intent(UserLessonsActivity.this, FeedbacksActivity.class);
                intent.putExtra(LESSON, filteredLessons.get(position));
                startActivity(intent);
            }
        };
    }

    private OnClickListener getOpenFileListener() {
        return new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Lesson lesson = filteredLessons.get(position);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(lesson.getFile()));
                startActivity(browserIntent);
            }
        };
    }

    private OnClickListener getZoomLinkListener() {
        return new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Lesson lesson = filteredLessons.get(position);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(lesson.getZoomLink()));
                startActivity(browserIntent);            }
        };
    }

    private OnClickListener getUploadFileListener() {
        return new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                AddAttachmentManager.openAttachmentIntent(UserLessonsActivity.this, position);
            }
        };
    }


    private void initFilters() {
        subject = findViewById(R.id.subject);
        date = findViewById(R.id.date);
        teacher = findViewById(R.id.teacher);
        level = findViewById(R.id.level);
        student = findViewById(R.id.student);
        school = findViewById(R.id.school);

        allFilter = new CheckedFilterItem(null, getString(R.string.all), "", true);
        subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(subjectsFilter);
                allFilter.setChecked(subjectSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.subject), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        subjectSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(datesFilter);
                allFilter.setChecked(dateSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.dateo), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        dateSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(levelsFilter);
                allFilter.setChecked(levelSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.level2), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        levelSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        teacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(teachersFilter);
                allFilter.setChecked(teacherSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.teacher1), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        teacherSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(schoolsFilter);
                allFilter.setChecked(schoolSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.school5), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        schoolSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });

        student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<CheckedFilterItem> items = new ArrayList<>(studentsFilter);
                allFilter.setChecked(studentSelectedFilter.getId() == null);
                items.add(0, allFilter);
                showBottomSheet(items, getString(R.string.students), new OnFilterClicked() {
                    @Override
                    public void onFilterClicked(CheckedFilterItem checkedFilterItem) {
                        studentSelectedFilter = checkedFilterItem;
                        arrangeListByFilters();
                    }
                });
            }
        });
    }

    private void showBottomSheet(List<CheckedFilterItem> items, String title, OnFilterClicked onFilterClicked) {
        FilterBottomSheet filterBottomSheet = new FilterBottomSheet(items, title, onFilterClicked);
        filterBottomSheet.show(getSupportFragmentManager(), "");
    }


    private OnClickListener getShowReviewsListener() {
        return new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Intent intent = new Intent(UserLessonsActivity.this, ReviewsActivity.class);
                intent.putExtra("lesson", filteredLessons.get(position));
                startActivity(intent);
            }
        };
    }

    private void getLessons(boolean isAdmin) {
        if (isAdmin) {
            fetchAllLesson();
        } else {
            fetchUserLessons();
        }
    }

    private void fetchUserLessons() {
        lessons.clear();
        filteredLessons.clear();
        resetFilters();
        for (Lesson lesson : SessionUser.currentUser.getLessons().values()) {
            addToFilters(lesson);
            if (shouldAddToFilterList(lesson)) {
                filteredLessons.add(lesson);
            }
            lessons.add(lesson);
        }
        swipeToRefresh.setRefreshing(false);
        list.getAdapter().notifyDataSetChanged();
    }

    private void resetFilters() {
        subjectsFilter.clear();
        schoolsFilter.clear();
        datesFilter.clear();
        teachersFilter.clear();
        levelsFilter.clear();
        studentsFilter.clear();
    }

    private void initFilterItems(Lesson lesson) {
        addToFilters(lesson);
        if (shouldAddToFilterList(lesson)) {
            filteredLessons.add(lesson);
        }
    }

    private void fetchAllLesson() {
        FirebaseDatabaseManager.getAllLessons(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    lessons.clear();
                    filteredLessons.clear();
                    List<DocumentSnapshot> documentSnapshots = task.getResult().getDocuments();
                    for (DocumentSnapshot snapshot: documentSnapshots) {
                        Lesson lesson = snapshot.toObject(Lesson.class);
                        initFilterItems(lesson);
                        lessons.add(lesson);
                    }
                    swipeToRefresh.setRefreshing(false);
                    list.getAdapter().notifyDataSetChanged();
                } else {
                    AlertDialogUtils.showAlertDialog(getApplicationContext(), getString(R.string.error), task.getException().getMessage());
                }
            }
        });
    }

    private void addToFilters(Lesson lesson) {
        subjectsFilter.add(new CheckedFilterItem(lesson.getSubject(), lesson.getSubject(), null, false));
        levelsFilter.add(new CheckedFilterItem(String.valueOf(lesson.getLevel()), String.valueOf(lesson.getLevel()), null, false));
        teachersFilter.add(new CheckedFilterItem(lesson.getTeacher().getId(), lesson.getTeacher().getName(), null, false));
        schoolsFilter.add(new CheckedFilterItem(String.valueOf(lesson.getSchool().getId()), lesson.getSchool().getName(),  lesson.getSchool().getLocation(), false));
        for (Student student1 : lesson.getStudents().values()) {
            studentsFilter.add(new CheckedFilterItem(String.valueOf(student1.getId()), student1.getName(),  null, false));
        }
        String date =  format.format(new Date(lesson.getTime()));
        datesFilter.add(new CheckedFilterItem(date, date,  null, false));
    }

    private boolean shouldAddToFilterList(Lesson lesson) {
        String date =  format.format(new Date(lesson.getTime()));

        return (lesson.getSubject().equals(subjectSelectedFilter.getId()) || subjectSelectedFilter.getId() == null)
                && (lesson.getTeacher().getId().equals(teacherSelectedFilter.getId()) || teacherSelectedFilter.getId() == null)
                && (String.valueOf(lesson.getLevel()).equals(levelSelectedFilter.getId()) || levelSelectedFilter.getId() == null)
                && (lesson.getSchool().getId().equals(schoolSelectedFilter.getId()) || schoolSelectedFilter.getId() == null)
                && (date.equals(dateSelectedFilter.getId()) || dateSelectedFilter.getId() == null);
    }

    private void arrangeListByFilters() {
        filteredLessons.clear();
        for (Lesson lesson : lessons) {
            if (shouldAddToFilterList(lesson)) {
                filteredLessons.add(lesson);
            }
        }
        lessonsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLessons(isAdmin);
    }

    private OnClickListener getDeleteListener() {
        return new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                AlertDialogUtils.showAlertConfirmationDialog(UserLessonsActivity.this,
                        getString(R.string.alert), getString(R.string.are_you_sure_you_want_to_remove_this_lesson),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Lesson removed = filteredLessons.remove(position);
                                SessionUser.currentUser.getLessons().remove(removed.getId());
                                fetchUserLessons();
                                FirebaseDatabaseManager.removeLesson(removed, new OnCompleteListener() {
                                    @Override
                                    public void onComplete(@NonNull Task task) {
                                        if (task.isSuccessful()) {
                                            if (SessionUser.isTeacher()) {
                                                FirebaseMessagingManager.sendNotification(UserLessonsActivity.this, removed.getId(), getString(R.string.lesson_removed)
                                                , getString(R.string.your_lesson_cancel, removed.getTeacher().getName(), removed.getSubject(), removed.getSchool().getName()));
                                            }
                                            Toast.makeText(UserLessonsActivity.this, R.string.lesson_is_removed_successfully, Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                                list.getAdapter().notifyItemRemoved(position);
                            }
                        });
            }
        };
    }

    private OnClickListener getShowStudentsListener() {
        return new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Intent intent = new Intent(UserLessonsActivity.this, UsersActivity.class);
                intent.putExtra(UsersActivity.LESSON, filteredLessons.get(position));
                startActivity(intent);
            }
        };
    }

    private OnClickListener getEditListener() {
        return new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Intent intent = new Intent(UserLessonsActivity.this, AddOrUpdateLessonActivity.class);
                intent.putExtra(AddOrUpdateLessonActivity.LESSON, filteredLessons.get(position));
                intent.putExtra(AddOrUpdateLessonActivity.IS_UPDATE, true);
                startActivity(intent);
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ProgressDialog progressDialog = new ProgressDialog(this);

        if (resultCode == RESULT_OK && data != null) {
            progressDialog.setTitle(R.string.uploading);
            progressDialog.show();
            Uri uri = data.getData();
            StorageReference ref = FirebaseStorage.getInstance().getReference().child(AddAttachmentManager.getFilePath());
            ref.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            progressDialog.hide();
                            Lesson lesson = filteredLessons.get(requestCode);
                            lesson.setFile(uri.toString());
                            FirebaseDatabaseManager.updateLesson(lesson, null);
                            lessonsAdapter.notifyItemChanged(requestCode);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.hide();
                        }
                    });
                    Toast.makeText(UserLessonsActivity.this, getString(R.string.file_uploaded), Toast.LENGTH_SHORT).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e)
                {
                    progressDialog.hide();
                    Toast.makeText(UserLessonsActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                    double progress = (100.0 * snapshot.getBytesTransferred())/snapshot.getTotalByteCount();
                    progressDialog.setMessage("Uploaded:" + (int)progress+"%");
                }
            });

        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString( isAdmin ? R.string.all_lessons: R.string.my_lessons);
    }
}