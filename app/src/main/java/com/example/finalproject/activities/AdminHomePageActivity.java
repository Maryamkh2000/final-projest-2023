package com.example.finalproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserProfileChangeRequest;

public class AdminHomePageActivity extends AppCompatActivity {


	private ImageView myv, signOutBT;
	private Button lessonsBT, teachersBtn, studentsBtn, schoolsBtn, statsBtn;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page_for_admin);
		UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
				.setDisplayName("admin").build();
		FirebaseAuth.getInstance().getCurrentUser().updateProfile(profileUpdates);
		signOutBT = (ImageView) findViewById(R.id.imageView3);
		lessonsBT = (Button) findViewById(R.id.classesBtn);
		teachersBtn = (Button) findViewById(R.id.teachersBtn);
		studentsBtn = (Button) findViewById(R.id.studentsBtn);
		schoolsBtn = (Button) findViewById(R.id.SchoolsBtn);
		statsBtn = (Button) findViewById(R.id.statistBtn);


		OnBackPressedCallback callback = new OnBackPressedCallback(true) {
			@Override
			public void handleOnBackPressed() {
				Intent intent = new Intent(AdminHomePageActivity.this, LoginActivity.class);
				startActivity(intent);           }
		};
		this.getOnBackPressedDispatcher().addCallback(this, callback);

		signOutBT.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FirebaseAuth.getInstance().signOut();
				finish();
			}
		});

		lessonsBT.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AdminHomePageActivity.this, UserLessonsActivity.class);
				intent.putExtra(UserLessonsActivity.IS_ADMIN, true);
				startActivity(intent);
			}
		});

		teachersBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AdminHomePageActivity.this, UsersActivity.class);
				intent.putExtra(UsersActivity.TEACHERS, true);
				startActivity(intent);
			}
		});

		studentsBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AdminHomePageActivity.this, UsersActivity.class);
				intent.putExtra(UsersActivity.TEACHERS, false);
				startActivity(intent);
			}
		});

		schoolsBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AdminHomePageActivity.this, AllSchoolsActivity.class);
				startActivity(intent);
			}
		});

		statsBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AdminHomePageActivity.this, StatisticActivity.class);
				startActivity(intent);
			}
		});
	
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
	
	