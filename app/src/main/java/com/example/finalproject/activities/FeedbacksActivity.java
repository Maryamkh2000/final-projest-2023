package com.example.finalproject.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.finalproject.Logic.Feedback;
import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.R;
import com.example.finalproject.adapters.FeedbacksAdapter;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.interfaces.OnClickListener;
import com.example.finalproject.interfaces.OnCompleteFeedbacksTask;
import com.example.finalproject.session.SessionUser;
import com.example.finalproject.utils.AlertDialogUtils;

import java.util.ArrayList;
import java.util.List;

public class FeedbacksActivity extends ListActivity {

    public static final String LESSON  = "LESSON";


    private FeedbacksAdapter adapter;
    private List<Feedback> feedbacks = new ArrayList<Feedback>();
    private Lesson lesson;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lesson =  (Lesson) getIntent().getSerializableExtra(LESSON);
        fetchData();
        adapter = new FeedbacksAdapter(this, feedbacks, new OnClickListener() {
            @Override
            public void onItemClicked(int position) {
                Feedback feedback = feedbacks.get(position);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(feedback.getAttachment()));
                startActivity(browserIntent);
            }
        });
        list.setAdapter(adapter);
    }

    private void fetchData() {
        swipeRefreshLayout.setRefreshing(true);
        if (SessionUser.isStudent()) {
            fetchFeedbacksByLessonId();
        } else {
            fetchTeacherFeedback();
        }
    }

    private void fetchTeacherFeedback() {
        FirebaseDatabaseManager.getFeedbackByUserId(SessionUser.getUserId(), new OnCompleteFeedbacksTask(){
            @Override
            public void OnCompleteFeedbacks(List<Feedback> feedbacks) {
                handleFeedbacks(feedbacks);
            }
        });
    }

    private void fetchFeedbacksByLessonId() {
        FirebaseDatabaseManager.getFeedbackByLesson(lesson.getId(), SessionUser.currentUser.getId(), new OnCompleteFeedbacksTask() {
            @Override
            public void OnCompleteFeedbacks(List<Feedback> feedbacks) {
                handleFeedbacks(feedbacks);
            }
        });
    }

    private void handleFeedbacks(List<Feedback> feedbacks) {
        swipeRefreshLayout.setRefreshing(false);
        if (feedbacks.isEmpty()) {
            AlertDialogUtils.showAlertDialog(FeedbacksActivity.this, getString(R.string.alert),
                    getString(R.string.no_result));
        } else {
            FeedbacksActivity.this.feedbacks.clear();
            FeedbacksActivity.this.feedbacks.addAll(feedbacks);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.feedbacks2);
    }

    @Override
    protected void reload() {
        super.reload();
        fetchData();
    }
}
