package com.example.finalproject.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;

import com.example.finalproject.R;
import com.example.finalproject.session.SessionUser;
import com.google.firebase.auth.FirebaseAuth;

public class TeacherHomePageActivity extends AppCompatActivity {

	private ImageView notification, signOutBT;
	private Button myProfile, myClassesBtn, myFeedbackBT;


	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_page_for_teacher_2);
		signOutBT = (ImageView) findViewById(R.id.sign_out);
		myClassesBtn = (Button) findViewById(R.id.myClassesBtn);
		notification = (ImageView) findViewById(R.id.notification);
		myProfile = (Button) findViewById(R.id.myProfile);
		myFeedbackBT = (Button) findViewById(R.id.myFeedbacks);

		OnBackPressedCallback callback = new OnBackPressedCallback(true) {
			@Override
			public void handleOnBackPressed() {
				Bundle extras = new Bundle();
				Intent intent = new Intent(TeacherHomePageActivity.this, LoginActivity.class);
				intent.putExtras(extras);
				startActivity(intent);           }
		};
		this.getOnBackPressedDispatcher().addCallback(this, callback);

		signOutBT.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FirebaseAuth.getInstance().signOut();
				finish();
			}
		});

		myClassesBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TeacherHomePageActivity.this , UserLessonsActivity.class);
				intent.putExtra(UserLessonsActivity.USER_ID, SessionUser.getUserId());
				intent.putExtra(UserLessonsActivity.IS_TEACHER, true);
				startActivity(intent);
			}
		});

		myProfile.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TeacherHomePageActivity.this , UserProfileActivity.class);
				startActivity(intent);
			}
		});

		notification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(TeacherHomePageActivity.this , NotificationsActivity.class);
				startActivity(intent);
			}
		});

		myFeedbackBT.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(TeacherHomePageActivity.this , FeedbacksActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
	
	