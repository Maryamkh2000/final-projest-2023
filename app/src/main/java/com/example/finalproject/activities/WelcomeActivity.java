package com.example.finalproject.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.example.finalproject.Logic.Student;
import com.example.finalproject.Logic.Teacher;
import com.example.finalproject.R;
import com.example.finalproject.database.FirebaseDatabaseManager;
import com.example.finalproject.session.SessionUser;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;

public class WelcomeActivity extends Activity {

    private Button getting_started;
    private Button who_are_we_;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        getting_started = (Button) findViewById(R.id.button);
        who_are_we_ = (Button) findViewById(R.id.button2);

        getting_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        who_are_we_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, InfoActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            if (SessionUser.isTeacher()) {
                FirebaseDatabaseManager.getTeacher(SessionUser.getUserId(), new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        SessionUser.currentUser = task.getResult().toObject(Teacher.class);
                        Intent intent = new Intent(WelcomeActivity.this, TeacherHomePageActivity.class);
                        startActivity(intent);
                    }
                }, true);
            } else if (SessionUser.isStudent()){
                FirebaseDatabaseManager.getStudent(SessionUser.getUserId(), new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        SessionUser.currentUser = task.getResult().toObject(Student.class);
                        Intent intent = new Intent(WelcomeActivity.this, StudentHomePageActivity.class);
                        startActivity(intent);
                    }
                });
            } else {
                Intent intent = new Intent(WelcomeActivity.this, AdminHomePageActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
	
	