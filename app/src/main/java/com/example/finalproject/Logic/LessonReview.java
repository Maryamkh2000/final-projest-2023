package com.example.finalproject.Logic;

import java.io.Serializable;

public class LessonReview implements Serializable {
    private String id;
    private float rating;
    private String comment;
    private Student reviewer;

    public LessonReview(String id, float rating, String comment, Student reviewer) {
        this.rating = rating;
        this.comment = comment;
        this.id = id;
        this.reviewer = reviewer;
    }

    public LessonReview(){}

    public void setReviewer(Student reviewer) {
        this.reviewer = reviewer;
    }

    public Student getReviewer() {
        return reviewer;
    }

    public String getId() {
        return id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
