package com.example.finalproject.Logic.Enums;

import java.io.Serializable;

public enum Step implements Serializable {
    Subjects, Schools;
}
