package com.example.finalproject.Logic;

import java.util.Objects;

public class CheckedFilterItem {
    private String id;

    public String getId() {
        return id;
    }

    public CheckedFilterItem(String id, String title, String subtitle, boolean isChecked) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.isChecked = isChecked;
    }

    public CheckedFilterItem(){}

    private String title;
    private String subtitle;
    private boolean isChecked;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckedFilterItem item = (CheckedFilterItem) o;
        return id == null || id.equals(item.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
