package com.example.finalproject.Logic;

import java.io.Serializable;

public class Notification implements Serializable {
    private String id;
    private String title;
    private String body;
    private String userId;
    private long time;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Notification(String id, String title, String body, long time, String userId) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.userId = userId;
        this.time = time;
    }

    public Notification(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
