package com.example.finalproject.Logic;

public class Student extends User{

    public Student(){}

    public Student(String id, String name, String email, String phone, String region, String image, String gender, String age) {
        super(id, name, email, phone, region, image, gender, age);
    }

    public Student(String id) {
        super(id);
    }

    public Student copy() {
        return new Student(id, name, email, phone, region, imageUrl, gender, age);
    }
}
