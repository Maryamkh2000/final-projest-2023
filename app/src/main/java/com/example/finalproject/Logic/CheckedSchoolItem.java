package com.example.finalproject.Logic;

public class CheckedSchoolItem {
    private boolean isChecked;
    private School school;
    private float distance;

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public CheckedSchoolItem(boolean isChecked, School school) {
        this.isChecked = isChecked;
        this.school = school;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
