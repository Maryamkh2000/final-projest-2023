package com.example.finalproject.Logic;

import java.util.Map;

public class Subjects {
    private Map<String, String> types;

    public Map<String, String> getTypes() {
        return types;
    }

    public void setTypes(Map<String, String> types) {
        this.types = types;
    }

}
