package com.example.finalproject.Logic;


import java.io.Serializable;
import java.util.Objects;

public class School implements Serializable {

    private String id;
    private String name;
    private String location;
    private int openingHour;
    private int closingHour;
    private GeoPoint geoPoint;

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public School(String id, String name, String location, GeoPoint geoPoint, int openingHour, int closingHour) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.geoPoint = geoPoint;
        this.openingHour = openingHour;
        this.closingHour = closingHour;
    }
    public School(){

    }

    public School(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getOpeningHour() {
        return openingHour;
    }

    public void setOpeningHour(int openingHour) {
        this.openingHour = openingHour;
    }

    public int getClosingHour() {
        return closingHour;
    }

    public void setClosingHour(int closingHour) {
        this.closingHour = closingHour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        School school = (School) o;
        return id.equals(school.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
