package com.example.finalproject.Logic;

public class CheckedTeacherItem {
    private boolean isChecked;
    private Teacher teacher;

    public CheckedTeacherItem(boolean isChecked, Teacher teacher) {
        this.isChecked = isChecked;
        this.teacher = teacher;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
