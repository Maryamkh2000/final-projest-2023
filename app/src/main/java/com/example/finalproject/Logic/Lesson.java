package com.example.finalproject.Logic;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Lesson implements Serializable {

    private int duration;
    private String id;
    private Teacher teacher;
    private String briefInformation;
    private long time;
    private School school;
    private int level;
    private String file;
    private double price;

    public String getZoomLink() {
        return zoomLink;
    }

    public void setZoomLink(String zoomLink) {
        this.zoomLink = zoomLink;
    }

    private String zoomLink;

    public Lesson(String id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStudentsLimit() {
        return studentsLimit;
    }

    public void setStudentsLimit(int studentsLimit) {
        this.studentsLimit = studentsLimit;
    }

    private int studentsLimit = Integer.MAX_VALUE;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Map<String, LessonReview> getLessonReviews() {
        return lessonReviews;
    }

    private Map<String, Student> students = new HashMap<>();
    private String subject;
    private Map<String, LessonReview> lessonReviews = new HashMap<>();


    public Lesson(String id, int level, String briefInformation, Teacher teacher, Map<String, Student> students, long time, String subject, int duration, School school, Map<String, LessonReview> lessonReviews,
                  String file, double price, int studentsLimit, String zoomLink) {
        this.teacher = teacher;
        this.id = id;
        this.level = level;
        this.briefInformation = briefInformation;
        this.subject = subject;
        this.duration = duration;
        this.time = time;
        this.school = school;
        this.lessonReviews = lessonReviews;
        this.file = file;
        this.price = price;
        this.studentsLimit = studentsLimit;
        this.zoomLink = zoomLink;
        this.students = students;
    }

    public Lesson(){}

    public void setStudents(Map<String, Student> students) {
        this.students = students;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setLessonReviews(Map<String, LessonReview> lessonReviews) {
        this.lessonReviews = lessonReviews;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Map<String ,Student> getStudents() {
        return students;
    }

    public String getBriefInformation() {
        return briefInformation;
    }

    public void setBriefInformation(String briefInformation) {
        this.briefInformation = briefInformation;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int  getDuration() {
        return duration;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson aLesson = (Lesson) o;
        return id.equals(aLesson.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Lesson copy() {
        return new Lesson(id, level, briefInformation, teacher, new HashMap<>()
                ,time, subject, duration, school, lessonReviews, file, price, studentsLimit, zoomLink);
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}