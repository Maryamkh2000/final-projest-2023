package com.example.finalproject.Logic;

import android.util.Log;
import androidx.annotation.NonNull;
import com.example.finalproject.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import com.example.finalproject.Logic.Enums.Gender;
import com.example.finalproject.Logic.Enums.Region;
import com.example.finalproject.Logic.Enums.SubjectEnum;

public class SysData {

    private static final String TAG = "SysData";


    private static SysData instance;
    static ArrayList<Teacher> teachers;
    static ArrayList<Student> students;
    static ArrayList<User> users;
    static ArrayList<Lesson> lessons;
    static ArrayList<Review> reviews;
    static ArrayList<Message> messages;
    static ArrayList<Room> rooms;
    static ArrayList<School> schools;


    static FirebaseFirestore db = FirebaseFirestore.getInstance();




    public SysData() {

    }

    public static synchronized SysData getInstance() {
        if (null == instance) {
            instance = new SysData();
            teachers = new ArrayList<Teacher>();
//            teachers.add(new Teacher("Saeed", R.drawable.male_boy_person_people_avatar_icon_159358, "Arabic"));
//            teachers.add(new Teacher("Hamzi", R.drawable.male_people_avatar_man_boy_curly_hair_icon_159362, "Biology"));
//            teachers.add(new Teacher("Sara", R.drawable.female_woman_avatar_people_person_white_tone_icon_159370, "Art"));
//            teachers.add(new Teacher("Sami", R.drawable.male_boy_person_people_avatar_icon_159358, "Sciences"));
//            teachers.add(new Teacher("Raghad", R.drawable.female_woman_avatar_people_person_white_tone_icon_159370, "English"));
//            teachers.add(new Teacher("Rina", R.drawable.female_woman_person_people_avatar_icon_159366, "Computers"));
//            teachers.add(new Teacher("Maher", R.drawable.male_people_avatar_man_boy_curly_hair_icon_159362, "Hebrew"));
//            teachers.add(new Teacher("Khaled", R.drawable.male_boy_person_people_avatar_white_tone_icon_159368, "Math"));
//            students = new ArrayList<Student>();
//            students.add(new Student("Adam", "First Class", R.drawable.male_boy_person_people_avatar_icon_159358));
//            students.add(new Student("Laila", "Fifth Class", R.drawable.female_woman_avatar_people_person_white_tone_icon_159370));
//            students.add(new Student("Mousa", "Second Class", R.drawable.male_boy_person_people_avatar_white_tone_icon_159368));
//            students.add(new Student("Salma", "Sixth Class", R.drawable.female_woman_person_people_avatar_icon_159366));
//            students.add(new Student("Aisha", "Fourth Class", R.drawable.female_woman_person_people_avatar_user_white_tone_icon_159359));
//            students.add(new Student("Mohammad", "Third Class", R.drawable.male_people_avatar_man_boy_curly_hair_icon_159362));
//            students.add(new Student("Omar", "Fifth Class", R.drawable.male_people_avatar_man_boy_curly_hair_icon_159362));
            users = new ArrayList<User>();
            lessons = new ArrayList<Lesson>();

            Task<QuerySnapshot> classes1 = db.collection("Classes")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                }
                            } else {
                                Log.d(TAG, "Error getting documents: ", task.getException());
                            }
                        }
                    });


//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18, 15,00,00)), "Arabic", "Third Class" ));
//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18,16,30,00)), "Math", "First Class" ));
//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18,12,00,00)), "Geography", "Fifth Class" ));
//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18,8,00,00)), "Sciences", "Second Class" ));
//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18,16,00,00)), "English", "Third Class" ));
//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18,17,30,00)), "Computers", "Sixth Class" ));
//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18,17,00,00)), "Hebrew", "Sixth Class" ));
//            lessons.add(new Lesson(new Timestamp(new Date(2023,3,18, 18,00,00)), "Arabic", "Fourth Class" ));
//            reviews = new ArrayList<Review>();
////            reviews.add(new Review(R.drawable.number_circle_four_icon_172323, new User("Dana"), "this class is amazing i learned a lot", new Class("Math class with teacher khaled")));
//  //          reviews.add(new Review(R.drawable.number_circle_one_icon_172321, new User("Yosef"),"the chairs were not comfortable", new Class("Art Class with teacher Tala")));
//    //        reviews.add(new Review(R.drawable.number_circle_three_icon_172318, new User("Jameel"), "nice class ", new Class("Science class")));
//      //      reviews.add(new Review(R.drawable.number_circle_five_icon_172324, new User("Maryam"), "i understand every thing for exam now",new Class("Math class in eastern School") ));
//            messages = new ArrayList<Message>();
//            messages.add(new Message(new User("Hajar", R.drawable.female_woman_avatar_people_person_white_tone_icon_159370), "Math Class Homework", "Hi, can you add me to math class in sunday"));
//            messages.add(new Message(new User("Tamer", R.drawable.male_boy_person_people_avatar_icon_159358), "English Exam", "Hello admin, can you add a new class for English exam"));
//            messages.add(new Message(new User("Rani", R.drawable.male_people_avatar_man_boy_curly_hair_icon_159362), "Delay my class", "i want to delay my class to monday morning"));
//            messages.add(new Message(new User("Maram", R.drawable.female_woman_person_people_avatar_icon_159366), "updating student", "Hi, can you change my age in the app"));
//            schools = new ArrayList<School>();
//            //School AlnajahSchool = new School("Alnajah School", "East", new Timestamp(new Date(2023,5,18,8,30,00)), new Timestamp(new Date(2023,5,18,18, 00,00)));
//            //schools.add(AlnajahSchool);
//            School AlraziSchool =  new School("Alrazi School", "West", new Timestamp(new Date(2023,5,18,9,00,00)), new Timestamp(new Date(2023,5,18,17,00,00)));
//            schools.add(AlraziSchool);
//            School EasternSchool = new School("Eastern School", "East", new Timestamp(new Date(2023,5,18,8,30,00)), new Timestamp(new Date(2023,5,18,16,30,00)));
//            schools.add(EasternSchool);
//            School IbnSinaSchool = new School("Ibn Sina School", "North", new Timestamp(new Date(2023,5,18,8,00,00)), new Timestamp(new Date(2023,5,18,16,00,00)));
//            schools.add(IbnSinaSchool);
//            School WesternSchool = new School("Western School", "West", new Timestamp(new Date(2023,5,18,10,00,00)), new Timestamp(new Date(2023,5,18,15,30,00)));
//            schools.add(WesternSchool);
//            School AlzaytoonSchool = new School("Alzaytoon School", "North", new Timestamp(new Date(2023,5,18,8,30,00)), new Timestamp(new Date(2023,5,18,16,00,00)));
//            schools.add(AlzaytoonSchool);
//            School InstituteOfEducation = new School("Institute of Education", "South", new Timestamp(new Date(2023,5,18,12,00,00)), new Timestamp(new Date(2023,5,18,20,00,00)));
//            schools.add(InstituteOfEducation);
//            School MathSchool = new School("Math School", "East", new Timestamp(new Date(2023,5,18,8,00,00)), new Timestamp(new Date(2023,5,18,15,30,00)));
//            schools.add(MathSchool);
//            School AlhodaSchool = new School("Alhoda School", "East", new Timestamp(new Date(2023,5,18,8,00,00)), new Timestamp(new Date(2023,5,18,18,00,00)));
//            schools.add(AlhodaSchool);
//            School AlamalSchool = new School("Alamal School", "West", new Timestamp(new Date(2023,5,18,9,00,00)), new Timestamp(new Date(2023,5,18,17,30,00)));
//            schools.add(AlamalSchool);
//            School AlsalamSchool = new School("Alsalam School", "East", new Timestamp(new Date(2023,5,18,8,30,00)), new Timestamp(new Date(2023,5,18,16,30,00)));
//            schools.add(AlsalamSchool);
//            School AnsarSchool = new School("Ansar School", "North", new Timestamp(new Date(2023,5,18,8,30,00)), new Timestamp(new Date(2023,5,18,16,30,00)));
//            schools.add(AnsarSchool);
//            School OmarInstitute = new School("Omar Institute", "West", new Timestamp(new Date(2023,5,18,10,00,00)), new Timestamp(new Date(2023,5,18,15,00,00)));
//            schools.add(OmarInstitute);
//            School NoorAcademy = new School("Noor Academy", "North", new Timestamp(new Date(2023,5,18,8,00,00)), new Timestamp(new Date(2023,5,18,16,00,00)));
//            schools.add(NoorAcademy);
//            School AlazharSchool = new School("Alazhar School", "East", new Timestamp(new Date(2023,5,18,8,30,00)), new Timestamp(new Date(2023,5,18,15,00,00)));
//            schools.add(AlazharSchool);
//
//            rooms = new ArrayList<Room>();
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//            rooms.add(new Room(AlnajahSchool, 20, 2, 5));
//
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//            rooms.add(new Room(AlraziSchool, 20, 2, 5));
//
//
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//            rooms.add(new Room(EasternSchool, 20, 2, 5));
//
//
//            rooms.add(new Room(IbnSinaSchool, 20, 2, 5));
//
//            rooms.add(new Room(WesternSchool, 20, 2, 5));
//
//            rooms.add(new Room(AlzaytoonSchool, 20, 2, 5));
//
//            rooms.add(new Room(InstituteOfEducation, 20, 2, 5));
//
//            rooms.add(new Room(MathSchool, 20, 2, 5));



        }
        return instance;
    }

    public  ArrayList<Teacher> getTeachers() {
        return teachers;
    }

    public  void setTeachers(ArrayList<Teacher> teachers) {
        SysData.teachers = teachers;
    }


    public  ArrayList<Student> getStudents() {
        return students;
    }

    public  void setStudents(ArrayList<Student> students) {
        SysData.students = students;
    }

    public  ArrayList<User> getUsers() {
        return users;
    }

    public  void setUsers(ArrayList<User> users) {
        SysData.users = users;
    }

    public  ArrayList<Lesson> getClasses() {
        return lessons;
    }

    public void setClasses(ArrayList<Lesson> lessons) {
        SysData.lessons = lessons;
    }

    public ArrayList<Review> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Review> reviews) {
        SysData.reviews = reviews;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        SysData.messages = messages;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        SysData.rooms = rooms;
    }
    public void addRoom (Room r){
        rooms.add(r);
    }
    public void addClass (Lesson c){
        lessons.add(c);
    }
    public void addStudent (Student s){
        students.add(s);
    }
    public void addTeacher (Teacher t){
        teachers.add(t);
    }
    public void addUser (User u){
        users.add(u);
    }
    public void removeRoom (Room r){
        rooms.remove(r);
    }
    public void removeClass (Lesson c){
        lessons.remove(c);
    }
    public void removeStudent (Student s){
        students.remove(s);
    }
    public void removeTeacher (Teacher t){
        teachers.remove(t);
    }
    public void removeUser (User u){
        users.remove(u);
    }

    public ArrayList<School> getSchools() {
        return schools;
    }

    public static void setSchools(ArrayList<School> schools) {
        SysData.schools = schools;
    }

    public ArrayList<Lesson> sortByTeacher (Teacher t ){
     ArrayList<Lesson> lessons = new ArrayList<>();
       for(int i = 0; i< SysData.lessons.size(); i++){
           if(SysData.lessons.get(i).getTeacher().equals(t)){
               lessons.add(SysData.lessons.get(i));
           }
       }
       return lessons;
    }

    public Teacher getTeacherByUserName(String UserName){

        ArrayList<Teacher> myTeachers = SysData.getInstance().getTeachers();

        for(Teacher teacher : myTeachers){
            if(teacher.getId().equals(UserName)){
                return teacher;
            }
        }

        return null;
    }

    //TODO
    public SubjectEnum getSubjectBySubjectName(String subjectName){

        if(subjectName.equals("Math")){
            return SubjectEnum.Math;
        }
        if(subjectName.equals("Sciences")){
            return SubjectEnum.Sciences;
        }
        if(subjectName.equals("Arabic")){
            return SubjectEnum.Arabic;
        }
        if(subjectName.equals("Hebrew")){
            return SubjectEnum.Hebrew;
        }
        if(subjectName.equals("English")){
            return SubjectEnum.English;
        }
        if(subjectName.equals("Art")){
            return SubjectEnum.Art;
        }
        if(subjectName.equals("Geography")){
            return SubjectEnum.Geography;
        }
        if(subjectName.equals("Chemistry")){
            return SubjectEnum.Chemistry;
        }
        if(subjectName.equals("Biology")){
            return SubjectEnum.Biology;
        }
        if(subjectName.equals("Physics")){
            return SubjectEnum.Physics;
        }
        if(subjectName.equals("Computers")){
            return SubjectEnum.Computers;
        }
        else {
            return null;
        }
    }

    public Room getRoomByRoomNumber(School school,int roomNumber){
        //ArrayList<Room> myRooms = school.getRooms();

//       for(Room room : myRooms){
//           if(room.getNumber() == roomNumber){
//               return room;
//           }
//       }
        return null;
    }
    public School getSchoolByName(String schoolName){

        ArrayList<School> mySchools = SysData.getInstance().getSchools();

        for(School school : mySchools){
            if(school.getName().equals(schoolName)){
                return school;
            }
        }
        return null;
    }

    public Timestamp getDateByString(int year, int month, int day){
        Timestamp time = new Timestamp(new Date(year, month, day));
        return time;
    }

    public Timestamp getHourByString(int hour, int minute){
        Timestamp time = new Timestamp(new Date(2023,5,19,hour, minute, 00));
        return time;
    }

    //TODO
    public ArrayList<Student> getStudentsByMultiText(String students){
        return null;
    }

    public Region getRegionByRegionName(String region){
        if(region.equals("North")){
            return Region.North;
        }
        if(region.equals("South")){
            return Region.South;
        }
        if(region.equals("East")){
            return Region.East;
        }
        if(region.equals("West")){
            return Region.West;
        }
        else {
            return null;
        }
    }

    public Gender getGenderByGenderName(String gender){
        if(gender.equals("Male")){
            return Gender.Male;
        }
        if(gender.equals("Female")){
            return Gender.Female;
        }
        else {
            return null;
        }
    }
}


