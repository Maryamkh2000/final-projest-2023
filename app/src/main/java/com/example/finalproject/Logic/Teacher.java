package com.example.finalproject.Logic;

public class Teacher extends User {

    public Teacher(String id, String name, String email, String phone, String region, String image, String gender, String age) {
        super(id, name, email, phone, region, image, gender, age);
    }

    Teacher() {}

    public Teacher(String id) {
        super(id);
    }

    public Teacher copy() {
        return new Teacher(id, name, email, phone, region, imageUrl, gender, age);
    }
}
