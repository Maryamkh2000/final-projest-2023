package com.example.finalproject.Logic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class User implements Serializable {
    protected String id;
    protected String name;
    protected Map<String, Lesson> lessons = new HashMap<>();
    protected String imageUrl;
    protected String email;
    protected String phone;
    protected String region;
    protected String gender;
    protected String age;

    public Map<String, Lesson> getLessons() {
        return lessons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User(String id, String name, String email, String phone, String region, String image, String gender, String age) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.phone = phone;
        this.region = region;
        this.imageUrl = image;
        this.gender = gender;
        this.age = age;

    }

    public User(String id, String age, String Image) {
        this.id = id;
        this.age = age;
        this.imageUrl = Image;
    }

    public User(String id, String image) {
        this.id = id;
        this.imageUrl = image;
    }



    public User(String id){
        this.id = id;
    }

    public User() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
