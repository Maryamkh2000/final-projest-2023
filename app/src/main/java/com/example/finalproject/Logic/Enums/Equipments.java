package com.example.finalproject.Logic.Enums;

public enum Equipments {
    Computer, Projector, TV, Conditioner, Games, Board, Closet, ArtTools, Hanger, TeacherTable, Tables, Chairs;
}
