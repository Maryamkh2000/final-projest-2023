package com.example.finalproject.Logic;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LessonRequest implements Serializable {

    private String id;
    private School school;
    private int level;
    private String subject;


    public LessonRequest(String id, School school, int level, String subject) {
        this.id = id;
        this.school = school;
        this.level = level;
        this.subject = subject;
    }

    public LessonRequest(){}


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LessonRequest aLesson = (LessonRequest) o;
        return id.equals(aLesson.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}