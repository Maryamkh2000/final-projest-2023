package com.example.finalproject.Logic;

import java.io.Serializable;

public class Feedback implements Serializable {

    private String id;
    private String message;
    private String attachment;
    private Teacher teacher;
    private Student student;
    private Lesson lesson;
    private String studentId;
    private String lessonId;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getLessonId() {
        return lessonId;
    }

    public void setLessonId(String lessonId) {
        this.lessonId = lessonId;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    private String teacherId;

    public Feedback(String id, String message, String attachment, String teacher, String student, String lesson) {
        this.id = id;
        this.message = message;
        this.attachment = attachment;
        this.teacherId = teacher;
        this.studentId = student;
        this.lessonId = lesson;
    }

    public Feedback(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
