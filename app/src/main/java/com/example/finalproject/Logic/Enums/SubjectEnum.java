package com.example.finalproject.Logic.Enums;

public enum SubjectEnum {
        Math, Sciences, Arabic, Hebrew, English, Art, Geography, Chemistry, Biology, Physics, Computers;
}
