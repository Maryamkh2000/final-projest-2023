package com.example.finalproject;


import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.Logic.Lesson;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class ResultClassesHolder extends RecyclerView.ViewHolder {

    TextView info1;
    Button classInfoBtn;
    Button joinBtn;
    RelativeLayout row;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    static Lesson myLesson;


    public ResultClassesHolder(@NonNull View itemView) {
        super(itemView);

        info1 = itemView.findViewById(R.id.info1);
        classInfoBtn = itemView.findViewById(R.id.classInfoBtn);
        joinBtn = itemView.findViewById(R.id.joinBtn);
        row = itemView.findViewById(R.id.row);


//        classInfoBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(itemView.getContext(),class_info_for_student.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                itemView.getContext().startActivity(intent);
//            }
//        });

        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addClassToMyClassesSubCollectionInUserDocument();
                addClassToMyClassesSubCollectionInStudentDocument();
                addStudentToClassSubCollection();
            }
        });


    }

    private void addStudentToClassSubCollection() {
//        Student student = new Student(userName, email,phone,region,type,"0",gender,age);
//
//        Task<QuerySnapshot> task = db.collection("Classes").whereEqualTo("briefInformation", info1.getText().toString()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
//            @Override
//            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
//                if (!queryDocumentSnapshots.isEmpty()) {
//                    queryDocumentSnapshots.getDocuments().get(0).getReference().collection("Students").add(student).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                        @Override
//                        public void onSuccess(DocumentReference documentReference) {
//                            Toast.makeText(itemView.getContext(),"You Joined Successfully!",Toast.LENGTH_SHORT).show();
//                        }
//                    }).addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            Toast.makeText(itemView.getContext(),"Failed To Add Student!",Toast.LENGTH_SHORT).show();
//
//                        }
//                    });
//                }
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//
//            }
//        });
//        while(!task.isComplete()){
//
//        }
    }

    private void addClassToMyClassesSubCollectionInStudentDocument() {
        db.collection("Classes").whereEqualTo("briefInformation", info1.getText().toString()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (!queryDocumentSnapshots.isEmpty()) {
                    myLesson = queryDocumentSnapshots.getDocuments().get(0).toObject(Lesson.class);
                }
                Task<QuerySnapshot> task = db.collection("Students").whereEqualTo("email", "email").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        queryDocumentSnapshots.getDocuments().get(0).getReference().collection("myClasses").add(myLesson).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Toast.makeText(itemView.getContext(),"You Joined Successfully!",Toast.LENGTH_SHORT).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(itemView.getContext(),"Failed To Add Class!",Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

                while(!task.isComplete()){

                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

    }

    private void addClassToMyClassesSubCollectionInUserDocument() {

        Task<QuerySnapshot> task = db.collection("Classes").whereEqualTo("briefInformation", info1.getText().toString()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (!queryDocumentSnapshots.isEmpty()) {
                    myLesson = queryDocumentSnapshots.getDocuments().get(0).toObject(Lesson.class);
                }
                    db.collection("Users").whereEqualTo("email", "email").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if (!queryDocumentSnapshots.isEmpty()) {
                                queryDocumentSnapshots.getDocuments().get(0).getReference().collection("myClasses").add(myLesson).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        Toast.makeText(itemView.getContext(),"You Joined Successfully!",Toast.LENGTH_SHORT).show();

                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(itemView.getContext(),"Failed To Add Class!",Toast.LENGTH_SHORT).show();

                                    }
                                });
                            }
                        }
            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
                }});


                while (!task.isComplete()) {

                }

            }
        }


