package com.example.finalproject.addattachmentmanager;

import android.app.Activity;
import android.content.Intent;

import com.example.finalproject.R;

import java.util.UUID;

public class AddAttachmentManager {

    public static void openAttachmentIntent(Activity activity, int requestCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.putExtra("requestCode", requestCode);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            activity.startActivityForResult(Intent.createChooser(intent, activity.getString(R.string.select_a_file)), requestCode);
        }catch (Exception e){}
    }

    public static String getFilePath() {
        return "files/"+ UUID.randomUUID().toString() + ".pdf";
    }
}
