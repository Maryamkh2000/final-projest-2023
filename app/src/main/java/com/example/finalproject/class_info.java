package com.example.finalproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;

public class class_info extends AppCompatActivity {

    private ImageView arrow5;
    private Spinner teacherSpinner;
    private MultiAutoCompleteTextView multiAutoCompleteTextView;
    private EditText descriptionEditText;
    private Spinner subjectSpinner;
    private Spinner durationSpinner;
    private Spinner ageSpinner;
    private Spinner schoolsSpinner;
    private Button datePickerBtn;
    private Button hourPickerBtn;
    private CheckBox everyWeekCheckBox;
    private CheckBox everyDayCheckBox;
    private Button updateBtn;
    private Button deleteBtn;
    private TextView date3;
    private TextView hour3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_info);

        arrow5 = findViewById(R.id.arrow5);
        teacherSpinner = findViewById(R.id.teacherSpinner);
        multiAutoCompleteTextView = findViewById(R.id.multiAutoCompleteTextView);
        descriptionEditText = findViewById(R.id.descriptionEditText);
        subjectSpinner = findViewById(R.id.subjectSpinner);
        durationSpinner = findViewById(R.id.durationSpinner);
        ageSpinner = findViewById(R.id.ageSpinner);
        schoolsSpinner = findViewById(R.id.schoolsSpinner);
        datePickerBtn = findViewById(R.id.datePickerBtn);
        hourPickerBtn = findViewById(R.id.hourPickerBtn);
        everyWeekCheckBox = findViewById(R.id.everyWeekCheckBox);
        everyDayCheckBox = findViewById(R.id.everyDayCheckBox);
        updateBtn = findViewById(R.id.updateBtn);
        deleteBtn = findViewById(R.id.deleteBtn);
        date3 = findViewById(R.id.date3);
        hour3 = findViewById(R.id.hour3);

        arrow5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        datePickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        hourPickerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
}