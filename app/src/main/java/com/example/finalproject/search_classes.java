package com.example.finalproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.finalproject.activities.StudentHomePageActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

import com.example.finalproject.Logic.Lesson;
import com.example.finalproject.Logic.Teacher;

public class search_classes extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public Spinner teacherSpinner1;
    public Spinner subjectSpinner1;
    public Spinner ageSpinner3;
    public Spinner schoolSpinner;
    public Button searchBtn;
    public ProgressBar progressBar;
    public TextView teacher;
    public TextView subject;
    public TextView age2;
    public TextView school2;
    public ImageView arrow6;
    ArrayList<Lesson> lessons;
    ArrayAdapter aa;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ArrayList<Teacher> teachers;
    ArrayList<String> teachersNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_classes);

        teacherSpinner1 = findViewById(R.id.teacherSpinner1);
        subjectSpinner1 = findViewById(R.id.subjectSpinner1);
        ageSpinner3 = findViewById(R.id.ageSpinner3);
        schoolSpinner = findViewById(R.id.schoolSpinner);
        searchBtn = findViewById(R.id.searchBtn);
        progressBar = findViewById(R.id.progressBar);
        teacher = findViewById(R.id.teacher);
        subject = findViewById(R.id.info1);
        age2 = findViewById(R.id.age2);
        school2 = findViewById(R.id.school2);
        arrow6 = findViewById(R.id.arrow6);

        getTeachers();

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchClass();
            }
        });

        arrow6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(search_classes.this, StudentHomePageActivity.class);
                startActivity(intent);
            }
        });


    }

    private void searchClass() {
        lessons = new ArrayList<Lesson>();

        Task<QuerySnapshot> task = db.collection("Classes").whereEqualTo("teacher", teacherSpinner1.getSelectedItem().toString()).whereEqualTo("subject", subjectSpinner1.getSelectedItem().toString()).whereEqualTo("age", ageSpinner3.getSelectedItem().toString()).whereEqualTo("school", schoolSpinner.getSelectedItem().toString()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if(!queryDocumentSnapshots.isEmpty()) {
                    for(int i = 0; i< queryDocumentSnapshots.size();i++){
                    Lesson lesson1 = queryDocumentSnapshots.getDocuments().get(i).toObject(Lesson.class);
                    System.out.println(lesson1);
                    lessons.add(lesson1);
                    }
                    Toast.makeText(search_classes.this, "classes are founded", Toast.LENGTH_SHORT).show();
                }
                teacherSpinner1.setVisibility(View.INVISIBLE);
                subjectSpinner1.setVisibility(View.INVISIBLE);
                ageSpinner3.setVisibility(View.INVISIBLE);
                schoolSpinner.setVisibility(View.INVISIBLE);
                searchBtn.setVisibility(View.INVISIBLE);
                teacher.setVisibility(View.INVISIBLE);
                subject.setVisibility(View.INVISIBLE);
                age2.setVisibility(View.INVISIBLE);
                school2.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);

                Intent intent = new Intent(search_classes.this, search_classes_results_activity.class);
                //intent.putParcelableArrayListExtra("classes", lessons);
                startActivity(intent);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(search_classes.this, "Query Failed", Toast.LENGTH_SHORT).show();
            }
        });

        while(!task.isComplete()){

        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getTeachers() {
        teachers = new ArrayList<Teacher>();

        CollectionReference teachersCollectionRef = db.collection("Teachers");

        Query teachersQuery = teachersCollectionRef;
        Task<QuerySnapshot> task = teachersQuery.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Teacher teacher = document.toObject(Teacher.class);
                        teachers.add(teacher);
                    }
                } else {
                    Toast.makeText(search_classes.this, "Query Failed", Toast.LENGTH_SHORT).show();
                }
                getTeachersNames();
                aa.notifyDataSetChanged();
            }
        });

        while(!task.isComplete()){

        }


    }

    private ArrayList<String> getTeachersNames() {
        teachersNames = new ArrayList<String>();


        for(Teacher teacher : teachers){
            teachersNames.add(teacher.getId());
        }

        teacherSpinner1.setOnItemSelectedListener(this);
        aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, teachersNames);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        teacherSpinner1.setAdapter(aa);

        return teachersNames;

    }
}