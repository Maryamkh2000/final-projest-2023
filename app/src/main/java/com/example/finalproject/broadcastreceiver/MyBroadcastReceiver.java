package com.example.finalproject.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.finalproject.R;
import com.example.finalproject.notificationsmanager.FirebaseMessagingManager;

public class MyBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String lessonId = intent.getStringExtra("id");
        FirebaseMessagingManager.sendNotification(context, lessonId,
                context.getString(R.string.the_lesson_will_start_in_10_minutes) );
    }
}